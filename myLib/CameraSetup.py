import tkinter as tk
from tkinter.constants import END, INSERT
from tkinter import filedialog, messagebox
from functools import partial
from PIL import Image, ImageTk
import cv2
from WebConfig import Web
from log import *
import requests
from threading import Thread


def ReadAllTextFile():
    root_log.info("\n\n============================= READ ENV: =============================")
    log_flag = 0
    try:
        file = open(ENVPATH + ".env", 'r+', encoding= 'utf-8')
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            #Kí tự đầu tiên trong file sau khi đọc sẽ làm ghi log bị lỗi. 
            #Cờ báo log_flag giúp loại bỏ kí tự đầu tiên không ghi vào log!
            if log_flag:
                root_log.info(str(data_file))
            log_flag = 1

            if(file_cursor == file.tell()):
                break
        pass
    except:
        root_log.error("LOI THUC THI FILE:\n" + str(traceback.format_exc()))
        pass
    finally:
        file.close()
        root_log.info("============================= CLOSE FILE: =============================")

def ReTextFile(old_line, new_line):
    retext = False
    try:
        file = open(ENVPATH + ".env", 'r+', encoding= 'utf-8')
        file_change = open(ENVPATH + "temp.txt", 'w', encoding= 'utf-8')
        file_change.truncate(0)
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            
            if(Find(data_file, old_line)):
                retext = True
                file_change.write(new_line + "\n")
                # print(new_line)
            else:
                file_change.write(data_file)
                # print(data_file)
                pass

            if(file_cursor == file.tell()):
                if retext == False:
                    file_change.write("\n" + new_line + "\n")
                break
        pass
    except:
        root_log.error("LOI THUC THI FILE:\n" + str(traceback.format_exc()))
        pass
    finally:
        file.close()
        file_change.close

    try:
        file_change = open(ENVPATH + ".env", 'w', encoding= 'utf-8')
        file = open(ENVPATH + "temp.txt", 'r', encoding= 'utf-8')
        file_change.truncate(0)
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            file_change.write(data_file)
            if(file_cursor == file.tell()):
                break
        pass
    finally:
        file.close()
        file_change.close

class DrawShapes(tk.Canvas, SETING):
    def __init__(self, text= None, root=None, master=None, **kwargs):
        super().__init__(master, **kwargs)
        SETING.__init__(self)

        self.set = False
        self.setbuff = False
        self.text = text
        self.roi_set = 100
        self.create_label()
        self.update_data()
        self.web = Web()
        self.live = False

    def update_data(self):
        self.setting_update()
        self.filename = self.result_folder
        self.segment_set = 6
        self.list_roi = [self.roi_1_default, self.roi_2_default, self.roi_3_default, self.roi_4_default, self.roi_5_default, self.roi_6_default, self.roi_segment]
        self.list_cam = [self.cam_1, self.cam_2, self.cam_3, self.cam_4, self.cam_5, self.cam_6]
        self.start = 0, 0
        self.stop  = 0, 0

    def create_label(self):
        # SAVE
        self.save_label = tk.Label(text="LƯU", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.save_label.place(x=1290, y= 90, width=70, height=50)
        self.save_label.bind('<Button-1>', self.save)

        # Quay lại
        self.set_im = Image.open('./img/back.png')
        self.tk_set = ImageTk.PhotoImage(self.set_im.resize((40,40), Image.ANTIALIAS))
        self.close_label = tk.Label(image=self.tk_set)
        self.close_label.place(x=1290, y= 10, width=70, height=50)
        self.close_label.bind('<Button-1>', self.close_wd)

        # ROI CAM1
        self.cam1_label = tk.Label(text="CAM 1", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.cam1_label.place(x=1290, y= 170, width=70, height=50)
        self.cam1_label.bind('<Button-1>', self.roi_cam1_change)

        # ROI CAM2
        self.cam2_label = tk.Label(text="CAM 2", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.cam2_label.place(x=1290, y= 230, width=70, height=50)
        self.cam2_label.bind('<Button-1>', self.roi_cam2_change)

        # ROI CAM3
        self.cam3_label = tk.Label(text="CAM 3", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.cam3_label.place(x=1290, y= 290, width=70, height=50)
        self.cam3_label.bind('<Button-1>', self.roi_cam3_change)

        # ROI CAM4
        self.cam4_label = tk.Label(text="CAM 4", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.cam4_label.place(x=1290, y= 350, width=70, height=50)
        self.cam4_label.bind('<Button-1>', self.roi_cam4_change)

        # ROI CAM5
        self.cam5_label = tk.Label(text="CAM 5", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.cam5_label.place(x=1290, y= 410, width=70, height=50)
        self.cam5_label.bind('<Button-1>', self.roi_cam5_change)

        # ROI CAM6
        self.cam6_label = tk.Label(text="CAM 6", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.cam6_label.place(x=1290, y= 470, width=70, height=50)
        self.cam6_label.bind('<Button-1>', self.roi_cam6_change)

        # ROI SEGMENT
        self.segment_label = tk.Label(text="SEGMENT", bg="#5b9bd5",fg="white", font=('Arial', 10))
        self.segment_label.place(x=1290, y= 530, width=70, height=50)
        self.segment_label.bind('<Button-1>', self.roi_segment_change)

        # KET QUA FOLDER
        self.result_label = tk.Label(text="Kết quả", bg="#5b9bd5",fg="white", font=('Arial', 11))
        self.result_label.place(x=1290, y= 590, width=70, height=50)
        self.result_label.bind('<Button-1>', self.chose_folder)

        # Cài đặt chi tiết:
        self.delete_label = tk.Label(text="Nâng cao", bg="#5b9bd5",fg="white", font=('Arial', 11))
        self.delete_label.place(x=1290, y= 650, width=70, height=50)
        self.delete_label.bind('<Button-1>', self.more_setting)

        # Đồng ý
        self.agree_label = tk.Label(text="Đồng ý", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.agree_label.place(x=1290, y= 730, width=70, height=30)
        self.agree_label.bind('<Button-1>', self.agree_save_buffer)
        
        # self.text = Text(self)
        # Cam
        self.cam_label = tk.Label(text="CAM:", bg="white", fg="black", font=('Arial', 13))
        self.cam_label.place(x=530, y= 730, width=70, height=30)
        
        #Live Video
        self.live_video_label = tk.Label(text="Live Video", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.live_video_label.place(x=10, y= 730, width=100, height=30)
        self.live_video_label.bind('<Button-1>', self.live_video)

        #Live Video stop
        self.live_video_stop_label = tk.Label(text="Dừng Video", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.live_video_stop_label.place(x=130, y= 730, width=100, height=30)
        self.live_video_stop_label.bind('<Button-1>', self.live_video_stop)

        self.text.pack()
        self.text.place(x=600, y=730, width=670, height=30)

    def display_exem(self):
        list_label = [self.cam1_label, self.cam2_label, self.cam3_label, self.cam4_label, self.cam5_label, self.cam6_label, self.segment_label]
        for index in range(0, 7):
            list_label[index].config(bg="#5b9bd5")
        list_label[self.roi_set].config(bg='#ff704d')

    def right_mouse_click(self, event):
        """fires when user clicks on the background ... creates a new rectangle"""
        self.start = event.x, event.y
        self.delete_rect("")
        
        root_log.info("PRESS: RIGHT MOUSE")
        if self.roi_set != self.segment_set:
            self.roi_rectangle = self.create_rectangle(*self.start, *self.start, width=5, outline="red")
            self.tag_bind(self.roi_rectangle, '<Button-1>', partial(self.on_click_rectangle, self.roi_rectangle))
            self.tag_bind(self.roi_rectangle, '<Button1-Motion>', self.on_right_mouse_move)
        else:
            self.segment_retangle = self.create_rectangle(*self.start, *self.start, width=5, outline="blue")
            self.tag_bind(self.segment_retangle, '<Button-1>', partial(self.on_click_rectangle, self.segment_retangle))
            self.tag_bind(self.segment_retangle, '<Button1-Motion>', self.on_right_mouse_move)

    def left_mouse_click(self, event):
        """fires when user clicks on the background ... creates a new rectangle"""
        root_log.info("PRESS: LEFT MOUSE")
        self.delete_rect("")

    def on_click_rectangle(self, tag, event):

        """fires when the user clicks on a rectangle ... edits the clicked on rectange"""
        if self.roi_set != self.segment_set:
            self.roi_rectangle = tag
        else:
            self.segment_retangle = tag

        x1, y1, x2, y2 = self.coords(tag)
        if abs(event.x-x1) < abs(event.x-x2):
            # opposing side was grabbed; swap the anchor and mobile side
            x1, x2 = x2, x1
        if abs(event.y-y1) < abs(event.y-y2):
            y1, y2 = y2, y1
        self.start = int(x1), int(y1)

    def on_right_mouse_move(self, event):
        """fires when the user drags the mouse ... resizes roi_rectanglely active rectangle"""
        if self.roi_set != self.segment_set:
            self.coords(self.roi_rectangle, *self.start, event.x, event.y)
        else:
            self.coords(self.segment_retangle, *self.start, event.x, event.y)
        self.stop = int(event.x), int(event.y)
        self.save_buff()

    def delete_rect(self, event):
        """fires when the user drags the mouse ... resizes roi_rectanglely active rectangle"""
        
        root_log.info("DELETE RECTANGLE!")
        try:
            if self.roi_set != self.segment_set:
                self.delete(self.roi_rectangle)
            else:
                self.delete(self.segment_retangle)
        except:
            root_log.error(str(traceback.format_exc()))
            pass

    def roi_segment_change(self, event):
        root_log.info("PRESS: SET SEGMENT: ")
        self.roi_set = self.segment_set
        self.display_exem()
        self.draw_rectangle()
        pass
    
    def Thread_live_video(self):
        cv2.namedWindow("Live Video", cv2.WINDOW_NORMAL)
        cv2.moveWindow("Live Video", 0, 0)
        while self.live:
            check, image_cam = self.cam_live.read()
            if check:
                cv2.imshow("Live Video", image_cam)
                cv2.resizeWindow("Live Video", 1280, 720)
                cv2.waitKey(25)
            else:
                self.live = False
        cv2.destroyWindow("Live Video")
        root_log.info("Live Video: STOPED")

    def live_video_stop(self, event):
        self.live = False

    def live_video(self, event):
        listCam = [self.cam_1, self.cam_2, self.cam_3, self.cam_4, self.cam_5, self.cam_6]

        if self.live:
            if messagebox.askokcancel("Thông báo:", "Video đang được phát, bạn có muốn tải lại video mới không?"):
                self.live = False
                time.sleep(0.1)
            else:
                return self.live

        if self.roi_set < 6:
            cam = listCam[self.roi_set]
        else:
            messagebox.showerror("ERROR!", "Bạn chưa chọn cam, hãy chọn cam trước khi muốn live video.")  
        try:
            root_log.info("Live Video: " + cam)
            http_cam = self.http_cam_covert(cam)
            # print(http_cam)
            if http_cam != "NONE":
                r = requests.get(http_cam + "/applogin.cgi", timeout= 2)
                if r.status_code == 200:
                    self.cam_live = cv2.VideoCapture(cam)
                if self.cam_live.isOpened():
                    check, image_cam = self.cam_live.read()
                else:
                    check = False
            else:
                self.cam_live = cv2.VideoCapture(cam)
                if self.cam_live.isOpened():
                    check, image_cam = self.cam_live.read()
                else:
                    check = False
        except:
            check = False
        if check:
            self.live = True
            self.pThread = Thread(target= self.Thread_live_video)
            self.pThread.start()
    
    def cam_read(self, cam, camName, roi_set):
        check = False
        path = "./myLib/camImg/" + camName + ".jpg"
        
        self.run = False
        self.roi_set = roi_set - 1
        self.display_exem()
        self.show_cam_info()

        if messagebox.askokcancel("Kiểm tra cam:", "Bạn có muốn tải hình ảnh mới từ cam không?"):
            try:
                root_log.info("UPDATE IMG: " + cam)
                http_cam = self.http_cam_covert(cam)
                # print(http_cam)
                if http_cam != "NONE":
                    r = requests.get(http_cam + "/applogin.cgi", timeout= 2)
                    if r.status_code == 200:
                        cam = cv2.VideoCapture(cam)
                    if cam.isOpened():
                        check, image_cam = cam.read()
                        cv2.imwrite(path, image_cam)
                
                        self.im = Image.open(path)
                        self.tk_im = ImageTk.PhotoImage(self.im)
                        self.im = self.create_image(0, 0,anchor="nw",image=self.tk_im)
                        self.tag_bind(self.im, '<Button-1>', self.right_mouse_click)
                        self.tag_bind(self.im, '<Button-3>', self.left_mouse_click)
                        self.tag_bind(self.im, '<Button1-Motion>', self.on_right_mouse_move) 
                    else:
                        check = False
                else:
                    cam = cv2.VideoCapture(cam)
                    if cam.isOpened():
                        check, image_cam = cam.read()
                        cv2.imwrite(path, image_cam)
                
                        self.im = Image.open(path)
                        self.tk_im = ImageTk.PhotoImage(self.im)
                        self.im = self.create_image(0, 0,anchor="nw",image=self.tk_im)
                        self.tag_bind(self.im, '<Button-1>', self.right_mouse_click)
                        self.tag_bind(self.im, '<Button-3>', self.left_mouse_click)
                        self.tag_bind(self.im, '<Button1-Motion>', self.on_right_mouse_move) 
                    else:
                        check = False
            except:
                check = False

        else:
            messagebox.showinfo("Cảnh báo:", "Cam chưa được kiểm tra, có thể cam bị lỗi mà bạn không biết!")
            root_log.info("DON'T UPDATE IMG!")
            check = True
        return check

    def show_cam_info(self):
        self.text.delete('0.0', END)
        self.text.insert(INSERT, self.list_cam[self.roi_set])

    def draw_rectangle(self):

        if self.roi_set != self.segment_set:
            try:
                self.delete(self.roi_rectangle)
                self.delete(self.segment_retangle)
            except:
                root_log.error("LOI VE RECTANGLE! LOI XAY RA KHI VE LAN DAU, CO THE BO QUA:\n")
                root_log.error(str(traceback.format_exc()))
                pass
            self.segment_retangle = self.create_rectangle(self.list_roi[self.segment_set], width=5, outline="blue")
            self.roi_rectangle = self.create_rectangle(self.list_roi[self.roi_set][0], self.list_roi[self.roi_set][1], self.list_roi[self.roi_set][2], self.list_roi[self.roi_set][3], width=5, outline="red")
            self.tag_bind(self.roi_rectangle, '<Button-1>', partial(self.on_click_rectangle, self.roi_rectangle))
            self.tag_bind(self.roi_rectangle, '<Button1-Motion>', self.on_right_mouse_move)
        else:
            try:
                self.delete(self.segment_retangle)
            except:
                root_log.error("LOI XOA RECTANGLE")
                root_log.error(str(traceback.format_exc()))
                pass
            self.segment_retangle = self.create_rectangle(self.list_roi[self.segment_set], width=5, outline="blue")
            self.tag_bind(self.segment_retangle, '<Button-1>', partial(self.on_click_rectangle, self.segment_retangle))
            self.tag_bind(self.segment_retangle, '<Button1-Motion>', self.on_right_mouse_move)
    
    def roi_cam1_change(self, event):
        root_log.info("PRESS: CAM1: " + self.cam_1)
        if self.cam_read(self.cam_1, NAME_CAM_1, 1):
            self.draw_rectangle()
        else:
            root_log.warning("CAM1 DOSE NOT EXIST!")
            messagebox.showinfo("Cảnh báo!", "CAM không tồn tại. Vui lòng kiểm tra lại kết nối cam và thử lại.")

    def roi_cam2_change(self, event):
        root_log.info("PRESS: CAM2: " + self.cam_2)
        if self.cam_read(self.cam_2, NAME_CAM_2, 2):
            self.draw_rectangle()
        else:
            root_log.warning("CAM2 DOSE NOT EXIST!")
            messagebox.showinfo("Cảnh báo!", "CAM không tồn tại. Vui lòng kiểm tra lại kết nối cam và thử lại.")
        pass

    def roi_cam3_change(self, event):
        root_log.info("PRESS: CAM3: " + self.cam_3)
        if self.cam_read(self.cam_3, NAME_CAM_3, 3):
            self.draw_rectangle()
        else:
            root_log.warning("CAM3 DOSE NOT EXIST!")
            messagebox.showinfo("Cảnh báo!", "CAM không tồn tại. Vui lòng kiểm tra lại kết nối cam và thử lại.")
        pass

    def roi_cam4_change(self, event):
        root_log.info("PRESS: CAM4: " + self.cam_4)
        if self.cam_read(self.cam_4, NAME_CAM_4, 4):
            self.draw_rectangle()
        else:
            root_log.warning("CAM4 DOSE NOT EXIST!")
            messagebox.showinfo("Cảnh báo!", "CAM không tồn tại. Vui lòng kiểm tra lại kết nối cam và thử lại.")
        pass

    def roi_cam5_change(self, event):
        root_log.info("PRESS: CAM5: " + self.cam_5)
        if self.cam_read(self.cam_5, NAME_CAM_5, 5):
            self.draw_rectangle()
        else:
            root_log.warning("CAM5 DOSE NOT EXIST!")
            messagebox.showinfo("Cảnh báo!", "CAM không tồn tại. Vui lòng kiểm tra lại kết nối cam và thử lại.")
        pass

    def roi_cam6_change(self, event):
        root_log.info("PRESS: CAM6: " + self.cam_6)
        if self.cam_read(self.cam_6, NAME_CAM_6, 6):
            self.draw_rectangle()
        else:
            root_log.warning("CAM6 DOSE NOT EXIST!")
            messagebox.showinfo("Cảnh báo!", "CAM không tồn tại. Vui lòng kiểm tra lại kết nối cam và thử lại.")
        pass

    def agree_save_buffer(self, event):
        root_log.info("PRESS: AGREE:")

        if self.start[0] > self.stop[0]:
            x_start = self.stop[0]
            x_stop = self.start[0]
        else:
            x_start = self.start[0]
            x_stop = self.stop[0]

        if self.start[1] > self.stop[1]:
            y_start = self.stop[1]
            y_stop = self.start[1]
        else:
            y_start = self.start[1]
            y_stop = self.stop[1]
            
        if self.roi_set < self.segment_set:
            #Get CAM from textbox
            cam = self.text.get("0.0", END)
            cam = cam.split("\n")[0]
            #Save cam to buffer
            self.list_cam[self.roi_set] = cam
            root_log.info("SET: " + "CAM" + str(self.roi_set + 1) + ": " + cam)
            #Save roi_cam to buffer
            if self.start[0] != 0 and self.start[1] != 0 and self.stop[0] != 0 and self.stop[1] != 0:
                self.list_roi[self.roi_set] = [x_start, y_start, x_stop, y_stop]
                root_log.info("SET: " + "ROI_" + str(self.roi_set + 1) + "_DEFAULT: " + str(self.list_roi[self.roi_set]))
        #Save roi_segment to buffer
        elif self.roi_set == self.segment_set:
            if self.start[0] != 0 and self.start[1] != 0 and self.stop[0] != 0 and self.stop[1] != 0:
                self.list_roi[self.roi_set] = [x_start, y_start, x_stop, y_stop]
                root_log.info("SET: " + "ROI_SEGMENT: " + str(self.list_roi[self.roi_set]))
        messagebox.showinfo("Thông báo", "Dữ liệu mới đã được lưu vào bộ nhớ đệm. \nBấm nút \"LƯU\" để lưu cài đặt mới.")
        pass 
    
    def save_buff(self):
        if self.start[0] > self.stop[0]:
            x_start = self.stop[0]
            x_stop = self.start[0]
        else:
            x_start = self.start[0]
            x_stop = self.stop[0]

        if self.start[1] > self.stop[1]:
            y_start = self.stop[1]
            y_stop = self.start[1]
        else:
            y_start = self.start[1]
            y_stop = self.stop[1]
            
        if self.roi_set < self.segment_set:
            cam = self.text.get("0.0", END)
            cam = cam.split("\n")[0]
            self.list_cam[self.roi_set] = cam
            if self.start[0] != 0 and self.start[1] != 0 and self.stop[0] != 0 and self.stop[1] != 0:
                self.list_roi[self.roi_set] = [x_start, y_start, x_stop, y_stop]
                root_log.info("ON MOVE: " + "ROI_" + str(self.roi_set + 1) + "_DEFAULT: " + str(self.list_roi[self.roi_set]))
        elif self.roi_set == self.segment_set:
            if self.start[0] != 0 and self.start[1] != 0 and self.stop[0] != 0 and self.stop[1] != 0:
                self.list_roi[self.roi_set] = [x_start, y_start, x_stop, y_stop]
                root_log.info("ON MOVE: " + "ROI_SEGMENT: " + str(self.list_roi[self.roi_set]))

    def save(self, event):
        root_log.info("PRESS: SAVE")

        list_roi = ["ROI_1_DEFAULT", "ROI_2_DEFAULT", "ROI_3_DEFAULT", "ROI_4_DEFAULT", "ROI_5_DEFAULT", "ROI_6_DEFAULT"]
        list_cam = ["CAM_1", "CAM_2", "CAM_3", "CAM_4", "CAM_5", "CAM_6"]
        for index in range(0, 6):
            ReTextFile(list_cam[index], list_cam[index] + "=" + self.list_cam[index])
            roi = list_roi[index] + "=" + str(self.list_roi[index])
            roi = roi.replace("[", "")
            roi = roi.replace("]", "")
            roi = roi.replace(" ", "")
            ReTextFile(list_roi[index], roi)
        roi = "ROI_SEGMENT=" + str(self.list_roi[self.segment_set])
        roi = roi.replace("[", "")
        roi = roi.replace("]", "")
        roi = roi.replace(" ", "")
        ReTextFile("ROI_SEGMENT", roi)
        ReTextFile("RESULT_FOLDER", "RESULT_FOLDER" + "=" + self.filename)
        self.update_data()
        ReadAllTextFile()
        messagebox.showinfo("Thông báo", "Dữ liệu mới đã được lưu.")
        pass

    def chose_folder(self, event):
        root_log.info("PRESS: CHOSE FOLDER")
        self.filename = filedialog.askdirectory()
        if(self.filename != ''):
            self.filename = self.filename + "/"
            root_log.info("NEW RESULT FORDER: " + self.filename)
            messagebox.showinfo("Thư mục lưu kết quả", self.filename)
        else:
            root_log.info("CANCEL: CHOSE FOLDER!")
            self.filename = self.result_folder
            root_log.info("RESULT FORDER: " + self.filename)
            
    def close_wd(self, event):
        self.live = False
        root_log.info("PRESS: BACK")
        self.web.stop()
        self.set = False

    def more_setting(self, event):
        root_log.info("PRESS: MORE SETTING:")
        try:
            http_cam = self.http_cam_covert(self.list_cam[self.roi_set])
        except:
            root_log.error("LOI CAI DAT NANG CAO:\n" + str(traceback.format_exc()))
            messagebox.showinfo("Nhắc nhở:", "Bạn chưa chọn camera, hãy chọn camera trước khi nhấn cài đặt nâng cao!")

        if http_cam != "NONE":
            try:
                self.web.run(http_cam + "/applogin.cgi")
            except:
                pass
        else:
            messagebox.showinfo("Thông báo:", "Không thể mở cài đặt bổ sung do camera không tồn tại hoặc camera của bạn không được hỗ trợ. Vùi lòng kiểm tra lại camera!")
        
        

# def main():
#     c = DrawShapes(width=1366, height=768, cursor="cross")
#     c.pack()
#     c.mainloop()

# if __name__ == '__main__':
#     main()