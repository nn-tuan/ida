
import numpy as np
import torch
import torch.nn as nn
from torchvision import transforms as T
from efficientnet_pytorch import EfficientNet
import cv2
import PIL

class Classify:
    def __init__(self, path_model, device="cuda") -> None:

        self.device = device

        self.labels = {0 : "de", 1 : "ko"}

        self.pretrained_size = 224

        self.model_transfer = EfficientNet.from_pretrained('efficientnet-b0')

        # Freeze weights
        for param in self.model_transfer.parameters():
            param.requires_grad = False
        in_features = self.model_transfer._fc.in_features


        # Defining Dense top layers after the convolutional layers
        self.model_transfer._fc = nn.Sequential(
            nn.BatchNorm1d(num_features=in_features),    
            nn.Linear(in_features, 512),
            nn.ReLU(),
            nn.BatchNorm1d(512),
            nn.Linear(512, 128),
            nn.ReLU(),
            nn.BatchNorm1d(num_features=128),
            nn.Dropout(0.4),
            nn.Linear(128, 2),
            )

        self.model_transfer.load_state_dict(torch.load(path_model))
        
        if device == "cuda":
            self.model_transfer = self.model_transfer.cuda()

        self.tfms = T.Compose([
                T.Resize(size=(224,224)), 
                T.ToTensor(),
                T.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]), 
            ])

    def __call__(self, img):
        image = PIL.Image.fromarray(img)
        img_tensor = self.tfms(image)
        img_tensor = img_tensor.to(self.device).unsqueeze(0)
        self.model_transfer.eval()
        output = self.model_transfer(img_tensor).cpu()
        pred = output.data.max(1, keepdim=True)[1]
        pred = np.array(pred)[0][0]
        return self.labels[pred]

# classify = Classify('./data/model.pt')

# img = cv2.imread("myLib/244.jpg")

# print(classify(img))