import torch
import torch.nn as nn
import torchvision.transforms as transforms
import numpy as np
import cv2
import PIL

def get_vgg_layers(config, batch_norm):

    layers = []
    in_channels = 3

    for c in config:
        assert c == 'M' or isinstance(c, int)
        if c == 'M':
            layers += [nn.MaxPool2d(kernel_size=2)]
        else:
            conv2d = nn.Conv2d(in_channels, c, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(c), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = c

    return nn.Sequential(*layers)

class VGG(nn.Module):
    def __init__(self, features, output_dim):
        super().__init__()

        self.features = features

        self.avgpool = nn.AdaptiveAvgPool2d(7)

        self.classifier = nn.Sequential(
            nn.Linear(512 * 7 * 7, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(0.5),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(0.5),
            nn.Linear(4096, output_dim),
        )

    def forward(self, x):
        x = self.features(x)
        x = self.avgpool(x)
        h = x.view(x.shape[0], -1)
        x = self.classifier(h)
        return x, h

class Classify:
    def __init__(self, path_model, device="gpu"):
        # vgg11_config = [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M']
        vgg11_config = [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M']
        vgg11_layers = get_vgg_layers(vgg11_config, batch_norm=True)
        OUTPUT_DIM = 2
        device = torch.device('cuda' if torch.cuda.is_available() and device == "gpu" else 'cpu')
        self.model = VGG(vgg11_layers, OUTPUT_DIM)
        self.model.load_state_dict(torch.load(path_model, map_location=device))
        self.pretrained_size = 224
        pretrained_means = [0.485, 0.456, 0.406]
        pretrained_stds = [0.229, 0.224, 0.225]

        self.test_transforms = transforms.Compose([
                                   transforms.Resize(self.pretrained_size),
                                   transforms.ToTensor(),
                                   transforms.Normalize(mean=pretrained_means,
                                                        std=pretrained_stds)
                               ])
        self.labels = {0 : "de", 1 : "ko"}
        
    def __call__(self, img):
        img = cv2.resize(img, (self.pretrained_size, self.pretrained_size))
        image = PIL.Image.fromarray(img)
        image = self.test_transforms(image)
        image = torch.as_tensor(np.array(image).reshape(1, 3, self.pretrained_size, self.pretrained_size))
        self.model.eval()
        with torch.no_grad():
            y_pred, _ = self.model(image)
            id_ = np.argmax(np.array(y_pred))
            return self.labels[id_]


# clas = Classify("./data/vgg16.pt")
# print("success")