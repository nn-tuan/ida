from threading import Thread
from multiprocessing import Queue

from Segmen import Segmemtation
from log import *
from MakeDir import *
import RS232Module

class CamProcess(Thread):
    """
        Mỗi CamProcess cho phép 2 cam xử lý đọc, lưu video:

        CamProcess(cam1, cam2, roi_cam1, roi_cam2, video_save_folder, name_of_cam_save_file...)
            + cam1: Truyền vào địa chỉ video cần xử lý or URL của camera.
            + cam2: Truyền vào địa chỉ video cần xử lý or URL của camera.
            + roi_cam: Vùng ROI (vùng xử lý đè vạch) của mỗi cam. Vùng này trong class không gọi đến, tuy nhiên sẽ được ImageProcess lấy để xử lý.
            + video_save_folder: Địa chỉ thư mục lưu kết quả video thu được. Các tạo thư mục xem module MakeDir.py
            + cam1Name, cam2Name: Tên của mỗi cam dùng để savevideo, lưu ảnh...
    """
    def __init__(self, cam1, cam2, roi1, roi2, videoFolder, cam1Name, cam2Name, log_folder, root_log_folder):
        Thread.__init__(self)
        self.cam1 = cam1
        self.roi1 = roi1
        self.cam2 = cam2
        self.roi2 = roi2
        self.videoFolder = videoFolder 
        self.cam1Name = cam1Name
        self.cam2Name = cam2Name

        self.queue_cam1 = Queue()
        self.queue_cam2 = Queue()

        self.isRunning  = False
        # self.idProcess  = Value("i")
        self.error = True

        # Cờ báo hoạt động của các cam.
        self.cam1Run    = False
        self.cam2Run    = False
        self.log_folder = log_folder
        self.root_log_folder = root_log_folder
        
        

    def run(self):
        # self.idProcess.value = os.getpid()
        self.isRunning = False
        self.error = False

        """
        logging.basicConfig(filename= self.root_log_folder + LOGNAME, level=logging.DEBUG, format= '[%(asctime)s]:[%(levelname)s]:%(message)s', datefmt= '%Y-%m-%d %H:%M:%S')
        
        baithi_logger = logging.getLogger('CAMProcess')
        CreatLog(self.log_folder + LOGNAME, baithi_logger, 'a', True, False)
        baithi_log = RootLogDef(baithi_logger)
        """
        baithi_log.info("   *** Process: START ***")
        # strInfo = "   +ID Process: " + str(self.idProcess.value) + ": " +  self.cam1Name +  ", " + self.cam2Name
        strInfo = "   +ID Process: " +  self.cam1Name +  ", " + self.cam2Name
        baithi_log.info(strInfo)
        try:
            # cam_1 = cv2.VideoCapture(self.cam1)
            self.cam1.set(cv2.CAP_PROP_BUFFERSIZE, 2)  # set buffer size
            # cam_2 = cv2.VideoCapture(self.cam2)
            self.cam2.set(cv2.CAP_PROP_BUFFERSIZE, 2)  # set buffer size
        except:
            baithi_log.error(str(traceback.format_exc()))



        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
        out_1 = cv2.VideoWriter(self.videoFolder + self.cam1Name + "\\" +  self.cam1Name + ".avi", fourcc, FPS, VIDEO_SIZE)
        out_2 = cv2.VideoWriter(self.videoFolder + self.cam2Name + "\\" + self.cam2Name + ".avi", fourcc, FPS, VIDEO_SIZE)
        
        # Kiểm tra cam đã mở! 
            # Nếu 1 trong 2 cam mở => Start xử lý!
            # Nếu cả 2 cam lỗi: Dừng xử lý.
        
        try: 
            if self.cam1.isOpened():
                self.cam1Run = True
                strInfo = "   +STARTED: " + self.cam1Name
                baithi_log.info(strInfo)
            else:
                self.cam1Run = False
        except:
            self.cam1Run = False
        try:
            if self.cam2.isOpened():
                self.cam2Run = True
                strInfo = "   +STARTED: " + self.cam2Name
                baithi_log.info(strInfo)
            else:
                self.cam2Run = False
        except:
            self.cam2Run = False

        if self.cam1Run or self.cam2Run:
            self.isRunning = True
        else:
            time.sleep(0.5)
            self.stop()

        baithi_log.info("   *** Process: RUNNING ***")
        start1 = time.time()
        start2 = time.time()
        start_reader = time.time()
        
        """
            Vòng lặp liên tục:
                1. Đọc Frame ảnh từ CAM: 
                    Thời gian đọc phụ thuộc vào FPS của CAM & TIMEREADER.
                    Nếu muốn chương trình đọc liên tục chỉ phụ thuộc vào FPS của CAM hãy ẩn dòng start_reader = time.time()
                2. Kiểm tra Frame ảnh có được đọc: check1 - cam1 : check2 - cam2
                    Kiểm tra cờ (Flag) ISRESIZE. Nếu có RESIZE => VIDEOSIZE
                    Lưu Frame ảnh đọc được vào video
                    Sau một khoảng thời gian FPSMAXQUEUE sẽ lấy 1 Frame ảnh đưa vào Queue của mỗi cam để ImageProcess xử lý.
                3. Nếu một trong các cam bị lỗi (check1 or check2 = False):
                    Dừng đọc cam tương ứng, dừng lấy ảnh cho vào Queue tương ứng. Set cờ báo lỗi: cam1Run, cam2Run
                    Nếu 2 cam lỗi: set cờ dừng xử lý cho Process này! Buộc STOP!
        """
        while self.isRunning:
            try:
                if time.time() - start_reader >= TIMEREADER:
                    # print(time.time() - start_reader)
                    start_reader = time.time() # Ẩn dòng này nếu muốn CAM đọc liên tục!
                    try:
                        check1, frame1 = self.cam1.read()
                    except:
                        check1 = False
                    try:
                        check2, frame2 = self.cam2.read()
                    except:
                        check2 = False
                    # Xử lý cho cam1
                    if check1:
                        self.cam1Run = True
                        if ISRESIZE:
                            frame1 = cv2.resize(frame1, VIDEO_SIZE)
                        out_1.write(frame1)
                        if time.time() - start1 >= FPSMAXQUEUE:
                            if self.queue_cam1.qsize() == 0:
                                self.queue_cam1.put(frame1)
                            start1 = time.time()  
                    else:
                        if self.cam1Run == True:
                            self.cam1Run = False
                            strInfo = "Can't open: " + self.cam1Name
                            baithi_log.warning(strInfo)
                    # Xử lý cho cam2
                    if check2:
                        self.cam2Run = True
                        if ISRESIZE:
                            frame2 = cv2.resize(frame2, VIDEO_SIZE)
                        out_2.write(frame2)
                        if time.time() - start2 > FPSMAXQUEUE:
                            if self.queue_cam2.qsize() == 0:
                                self.queue_cam2.put(frame2)
                            start2 = time.time()  
                    else:
                        if self.cam2Run == True:
                            self.cam2Run = False
                            strInfo = "Can't open: " + self.cam2Name
                            baithi_log.warning(strInfo)
                    
                    if self.cam1Run == False and self.cam2Run == False:
                        if self.error == False:
                            # strInfo = "FORCE STOP: " + str(self.idProcess.value) + ": " +  self.cam1Name + ", " + self.cam2Name
                            strInfo = "FORCE STOP: " + ": " +  self.cam1Name + ", " + self.cam2Name
                            baithi_log.warning(strInfo)
                            baithi_log.warning("   +STOP: " + self.cam1Name)
                            baithi_log.warning("   +STOP: " + self.cam2Name)
                            baithi_log.warning("====================================================\n")
                            self.error = True
                        # self.isRunning = False
                else:
                    time.sleep(0.01)
                    pass
            except:
                baithi_log.error("LOI TRONG QUA TRINH THI:\n", str(traceback.format_exc()))

    def stop(self):
        self.isRunning = False
        self.queue_cam1.close()
        self.queue_cam2.close()
        self.error = True
        try:
            baithi_log.info("   +STOP: " + self.cam1Name)
            baithi_log.info("   +STOP: " + self.cam2Name)
        except Exception as ex:
            root_log.error("LOI STOP CAM: \n" + str(traceback.format_exc()))
            baithi_log.error("LOI STOP CAM: " + str(ex))
            
        # try:
        #     #os.kill(self.idProcess.value, signal.SIGBREAK)
        #     pass
        # except:
        #     baithi_log.info("   +STOPED!")
        #     pass


count = 0
class ThreadHand(Thread):
    def __init__(self, queue, saveFolder, baithi, ten, maTS):
        Thread.__init__(self)
        self.saveFolder = saveFolder
        self.baithi = baithi
        self.ten = ten
        self.maTS = maTS
        self.queue = queue
        self.ketqua = False
        self.isRunning = True
    def run(self):
        global count
        time_break = 0
        try:
            handleCheckLane = Segmemtation()
        except:
            root_log.error("Khong the tao class xu ly anh:\n" + str(traceback.format_exc()))
            baithi_log.error("Khong the tao class xu ly anh:\n" + str(traceback.format_exc()))
        while self.isRunning:
            if self.queue.qsize() > 0:
                try:
                    queue = self.queue.get(True, 1)
                    count += 1
                    # print(queue[2])
                    # start = time.time()
                    self.ketqua = handleCheckLane(queue[0], queue[1], self.saveFolder + queue[2], queue[2], count, self.baithi,
                                                    self.ten, self.maTS)
                    time_break = 0
                    # print(time.time() - start)
                except:
                    #Cho phép bỏ qua lỗi khi một Process đọc dữ liệu từ cam bị dừng đột ngột. 
                    #Hàm tự động STOP khi cả hai Process xử lý cam dừng lại hoặc Process xử lý hình ảnh bị lỗi.
                    root_log.error("Qua trinh xu ly anh loi:\n" + str(traceback.format_exc()))
                    time.sleep(1)
                    time_break += 1
                    if time_break >= 3:
                        root_log.error("Tu dong yeu cau dung xu ly anh:\n" + str(traceback.format_exc()))
                        self.isRunning = False
                    pass                
            else:
                time.sleep(0.001)

    def stop(self):
        self.queue.close()
        self.isRunning = False


class ImageThread(Thread):
    def __init__(self, saveFolder, baithi, ten, maTS):
        Thread.__init__(self)
        self.queue = Queue()
        self.saveFolder = saveFolder
        self.baithi = baithi
        self.ten = ten
        self.maTS = maTS

        self.isRunning = False
        self.ketqua = False
        self.isReady = False
        # self.idProcess = 0

        self.pThread1 = ThreadHand(self.queue, self.saveFolder, self.baithi, self.ten, self.maTS)
        self.pThread2 = ThreadHand(self.queue, self.saveFolder, self.baithi, self.ten, self.maTS)
        self.pThread3 = ThreadHand(self.queue, self.saveFolder, self.baithi, self.ten, self.maTS)

    def run(self):
        # self.idProcess = os.getpid()
        # baithi_log.info("   +SEGMEN Process: " + str(self.idProcess))
        baithi_log.info("   +SEGMEN Process")
        # print("   +SEGMEN Process:", self.idProcess)

        self.isRunning = True
        self.isReady = True
        
        self.pThread1.start()
        self.pThread2.start()
        self.pThread3.start()

        while self.isRunning:
            if self.pThread1.isRunning == False and self.pThread2.isRunning == False and self.pThread3.isRunning == False:
                self.isRunning = False
            else:
                time.sleep(0.1)
            pass

    def stop(self):
        self.pThread1.stop()
        self.pThread2.stop()
        self.pThread3.stop()
        self.isRunning = False
        self.queue.close()
        # baithi_log.info("   +STOP Image Process: " + str(self.idProcess))
        baithi_log.info("   +STOP: Image Process")
        # print("   +STOP Image Process:", self.idProcess)




"""
    Khai báo toàn cục handle của các Thread, Process phục vụ đọc, thu video và xử lý ảnh.
"""
pCam1 = pCam2   = CamProcess (None, None, None, None, None, None, None, None, None)
pImage          = ImageThread(None, None, None, None)



class IDAProcess(SETING):
    """
        Class xử lý của chương trình, liên kết quá trình xử lý, hiển thị trên Giao Diện và gửi dữ liệu qua RS232.
        Hàm Main liên tục gọi IDAProcess.run để lấy thông tin xử lý, hiển thị giao diện & gửi thông tin qua RS232.
    """
    def __init__(self):
        SETING.__init__(self)

        self.rs232 = RS232Module.RS232()
        self.data = ""
        self.rs232_state = ""

        self.result_connect = [KDCAMERR + str(1), KDCAMERR + str(2), KDCAMERR + str(3), KDCAMERR + str(4), KDCAMERR + str(5), KDCAMERR + str(6)]
        # Cờ báo kiểm tra cam để gửi qua RS232
        self.connect_buff    = [KDCAMERR + str(1), KDCAMERR + str(2), KDCAMERR + str(3), KDCAMERR + str(4), KDCAMERR + str(5), KDCAMERR + str(6)]
        
        self.is_camcheck    = False
        self.is_camchecking = False
        
        self.root_log_folder = ""
        self.update_seting()
        self.log_folder = ""
        
        self.cam_run = [None, None, None, None, None, None, None]
        
    def update_seting(self):
        self.setting_update()
        self.roi1 = self.roi_1_default
        self.roi2 = self.roi_2_default
        self.roi3 = self.roi_3_default
        self.roi4 = self.roi_4_default
        self.roi5 = self.roi_5_default
        self.roi6 = self.roi_6_default
        # self.log_folder = self.result_folder

    def set(self, baithi, mathisinh):
        """
            Hàm trung gian set giá trị cho Bài thi & mã thí sinh tương ứng.
        """
        if not baithi == "":
            self.rs232.thisinh.BaiThi = baithi
        if not mathisinh == "":
            self.rs232.thisinh.maTS = mathisinh
    
    # Hàm trung gian gửi giá trị đồng bộ quá trình chạy giữa giao diện và RS232.
    def LayOutData(self, data):
        self.data = data
        pass

    # Tạo thư mục lưu trữ dữ liệu trước khi bắt đầu bài thi!
    def StartFolder(self, baithi, mathisinh):
        global pLog
        folder = ""
        if baithi == BAITHISO8:
            folder = SO8FOLDER
        elif baithi == BAITHIZICZAC:
            folder = ZICZACFOLDER
        elif baithi == BAITHIQUAYVONG:
            folder = QUAYVONGFOLDER
        elif baithi == BAITHIHANGDINH:
            folder = HANGDINHFOLDER
        # print("MTS:", mathisinh)
        path = create_folder_result(mathisinh, self.result_folder)
        path_save_video = path + VIDEORECORDFOLDER + folder + "\\"
        path_save_image = path + DEVACHFOLDER + folder + "\\"
        path_result = [path_save_video, path_save_image, path]

        #Creat a log file
        # if self.log_folder == "":
        self.log_folder = path + LOGNAME
        try:
            baithi_logger.removeHandler(self.logHandler)
        except:
            root_log.error("LOI XOA LOG HANDLE:\n" + str(traceback.format_exc()))
            pass
        self.logHandler = CreatLog(self.log_folder, baithi_logger, 'a', True)

        baithi_log.info("\n\n\n================================================*** START: " + baithi + " ***================================================")
        return path_result    
    
    def StartBaiThi(self, baithi, ten, mathisinh):
        """
            Start bài thi:
                Bắt đầu khởi động CamProcess & ImageThread.
                Nếu Quá thời gian Timeout mà không khởi động được chương trình => Break và dừng Bài thi.
                    Thời gian cần cho các cam khởi động: 4 - 8s
                    Thời gian Timeout: ~ 50 * 0.2s = 10s
                return: 
                    True: Nếu một trong các cam hoạt động, CamProcess được khởi động.
                    False: Nếu tất cả các cam lỗi. 
        """
        global pCam1, pCam2
        global pImage
        stated = True
        timeout = 0


        path_result = self.StartFolder(baithi, mathisinh)
        path_save_video = path_result[0]
        path_save_image = path_result[1]
        self.log_folder = path_result[2]
        pImage = ImageThread(path_save_image, baithi, ten, mathisinh)
        pImage.start()
        if not baithi == BAITHIHANGDINH:
            pCam1 = CamProcess(self.cam_run[1], self.cam_run[2], self.roi1, self.roi2, path_save_video, NAME_CAM_1, NAME_CAM_2, self.log_folder, self.root_log_folder)
            pCam2 = CamProcess(self.cam_run[5], self.cam_run[6], self.roi5, self.roi6, path_save_video, NAME_CAM_5, NAME_CAM_6, self.log_folder, self.root_log_folder)
            pCam1.start()
            pCam2.start()
            # Tat ca bai thi da start, neu het timeout -> stop.
            while(pCam1.is_alive == False and pCam2.is_alive == False):
                pass
            while(pCam1.isRunning == False and pCam2.isRunning == False):
                time.sleep(0.2)
                timeout += 1
                if timeout > 10:
                    stated = False
                    baithi_log.info("Loi start bai thi: " + baithi) 
                    break
                pass
        else:
            pCam1 = CamProcess(self.cam_run[3], self.cam_run[4], self.roi3, self.roi4, path_save_video, NAME_CAM_3, NAME_CAM_4, self.log_folder, self.root_log_folder)
            pCam1.start()
            time.sleep(0.1)
            while(pCam1.isRunning == False):
                time.sleep(0.2)
                timeout += 1
                # print(timeout)
                if timeout > 10:
                    stated = False
                    baithi_log.info("Loi start bai thi: " + baithi) 
                    break
                pass
        if stated:
            baithi_log.info("===================*** Da start ***================================================\n")
        return stated

    def StopBaiThi(self):
        """
            Stop bài thi:
                Kiểm tra các Process và Thread đã khởi động.
                Nếu còn chạy (is_alive = True): Stop Process or Thread tương ứng.
                return: True
        """
        global pCam1, pCam2, pImage
        pProcessList = [pCam1, pCam2, pImage]
        try:
            baithi_log.info("=====================*** STOP ***==================================================")
            for pProcess in pProcessList:
                if pProcess.is_alive():
                    pProcess.stop()
                    time.sleep(0.1)
            baithi_log.info("====================*** DA STOP ***================================================")
        except Exception as ex:
            root_log.error("LOI TRONG QUA TRINH STOP BAI THI: \n" + str(traceback.format_exc()))
            baithi_log.error("LOI STOP KHONG XAC DINH: " + str(ex))
        return True

    # Khởi động lần đầu: Mở kết nối RS232. Nếu lỗi báo lỗi trên Console.
    def FirstRun(self):
        global pCam1, pCam2
        ports = RS232Module.SerialComList()
        root_log.info(ports)
        if len(ports) > 0:
            root_log.info("Mo COM doi tuong:")
            self.rs232.Open(COMDEFAULT)
    
    # Kiểm tra trạng thái các CAM trong khi bài thi đang diễn ra. Không có hiệu lực khi bài thi đã dừng lại!
    def KiemTraLoiCam(self):
        if self.rs232.thisinh.Thi == STARTED:
            self.is_camcheck = True
            # CheckCam while running
            if self.rs232.thisinh.BaiThi != BAITHIHANGDINH:
                if pCam1.cam1Run != True:
                    self.connect_buff[0] = KDCAMERR + str(1)
                else:
                    self.connect_buff[0] = KDCAMOK + str(1)
                if pCam1.cam2Run != True:
                    self.connect_buff[1] = KDCAMERR + str(2)
                else:
                    self.connect_buff[1] = KDCAMOK + str(2)
                if pCam2.cam1Run != True:
                    self.connect_buff[4] = KDCAMERR + str(5)
                else:
                    self.connect_buff[4] = KDCAMOK + str(5)
                if pCam2.cam2Run != True:
                    self.connect_buff[5] = KDCAMERR + str(6)
                else:
                    self.connect_buff[5] = KDCAMOK + str(6)
            else:
                if pCam1.cam1Run != True:
                    self.connect_buff[2] = KDCAMERR + str(3)
                else:
                    self.connect_buff[2] = KDCAMOK + str(3)
                if pCam1.cam2Run != True:
                    self.connect_buff[3] = KDCAMERR + str(4)
                else:
                    self.connect_buff[3] = KDCAMOK + str(4)       
            # Update result_buff => Send data to Rs232
            for index in range(0, 6):
                if self.result_connect[index] != self.connect_buff[index]:
                    self.result_connect[index] = self.connect_buff[index]
                    if self.rs232.is_ready:
                        if self.rs232.SerialWriteStr(self.result_connect[index]):
                            baithi_log.info("RS232: TRANSMIT: " + self.result_connect[index])
                        else:
                            #Loi cong COM!
                            baithi_log.warning("RS232: TRANSMIT: ERROR: " + self.result_connect[index])
                            pass
                    
        else:
            pass
    
    def KiemTraKetQua(self, baithi):
        """
        Hàm Kiểm tra kết quả của mỗi bài thi:
        Truyền vào: Tên bài thi tương ứng.
        Xử lý:
            Nếu chương trình xử lý cam lỗi: STOP bài thi.
            Chương trình diễn ra bình thường:
                Chuyển Frame ảnh từ Queue của mỗi cam => Queue của chương trình xử lý ảnh.
                Riêng bài thi HANGDINH hoạt động 1 pCam, các bài thi khác hoạt động 2 pCam.
                Kiểm tra các cờ báo lỗi của các Thread trong chương trình xử lý ảnh. Nếu pImage.Thread.ketqua = True: Báo lỗi.
        return: None
        """
        global pImage
        try:
        #Gui frame anh den Process xu ly de vach
            if pImage.isRunning == False:
                if self.rs232.thisinh.Thi != STOPED:
                    self.LayOutData(STOP)
                return 0
            if pCam1.is_alive == False and pCam2.is_alive == False:
                if self.rs232.thisinh.Thi != STOPED:
                    self.LayOutData(STOP)
                return 0
            if pCam1.error == True and pCam2.error == True:
                if self.rs232.thisinh.Thi != STOPED:
                    self.LayOutData(STOP)
                return 0
            # Xử lý đối với các bài thi không phải Hàng Đinh.
            if self.rs232.thisinh.BaiThi != BAITHIHANGDINH:
                if pImage.queue.qsize() == 0:
                    pCamList = [pCam1, pCam2]
                    for pCam in pCamList:
                        if pCam.queue_cam1.qsize() > 0:
                            frame = pCam.queue_cam1.get(True, 1)
                            queue = [frame, pCam.roi1, pCam.cam1Name]
                            pImage.queue.put(queue)
                        if pCam.queue_cam2.qsize() > 0:
                            frame = pCam.queue_cam2.get(True, 1)
                            queue = [frame, pCam.roi2, pCam.cam2Name]
                            pImage.queue.put(queue)
            # Xử lý riêng cho bài thi Hàng Đinh.
            else:
                if pImage.queue.qsize() == 0:
                    if pCam1.queue_cam1.qsize() > 0:
                        frame = pCam1.queue_cam1.get(True, 1)
                        queue = [frame, pCam1.roi1, pCam1.cam1Name]
                        pImage.queue.put(queue)
                    if pCam1.queue_cam2.qsize() > 0:
                        frame = pCam1.queue_cam2.get(True, 1)
                        queue = [frame, pCam1.roi2, pCam1.cam2Name]
                        pImage.queue.put(queue)
        except:
            root_log.error("LOI TRONG QUA TRINH KIEM TRA KET QUA:\n" + str(traceback.format_exc()))
            # print("Loi xu ly va kiem tra ket qua")
            pass
        # Kiểm tra kết quả xử lý đè vạch và gửi lỗi qua RS232
        try:
            if pImage.pThread1.ketqua == 1:
                baithi_log.info("RS232: TRANSMIT: " + DEVACH)
                self.rs232.SerialWriteStr(baithi + DEVACH)
                pImage.pThread1.ketqua = 0
            elif pImage.pThread1.ketqua == 2:
                baithi_log.info("RS232: TRANSMIT: " + DEDINH)
                self.rs232.SerialWriteStr(baithi + DEDINH)
                pImage.pThread1.ketqua = 0
            if pImage.pThread2.ketqua == 1:
                baithi_log.info("RS232: TRANSMIT: " + DEVACH)
                self.rs232.SerialWriteStr(baithi + DEVACH)
                pImage.pThread2.ketqua = 0
            elif pImage.pThread2.ketqua == 2:
                baithi_log.info("RS232: TRANSMIT: " + DEDINH)
                self.rs232.SerialWriteStr(baithi + DEDINH)
                pImage.pThread2.ketqua = 0
            if pImage.pThread3.ketqua == 1:
                baithi_log.info("RS232: TRANSMIT: " + DEVACH)
                self.rs232.SerialWriteStr(baithi + DEVACH)
                pImage.pThread3.ketqua = 0
            elif pImage.pThread3.ketqua == 2:
                baithi_log.info("RS232: TRANSMIT: " + DEDINH)
                self.rs232.SerialWriteStr(baithi + DEDINH)
                pImage.pThread3.ketqua = 0
        except:
            #Loi cong COM!
            baithi_log.warning("RS232: TRANSMIT: ERROR: " + DEVACH)
            pass
     
    def rs232_run(self):
        """
            Chương trình xử lý cho giao tiếp RS232 với chương trình:
            Gọi hàm RS232.run:
                Nếu trả về START: Khởi động bài thi tương ứng.
                    Cho phép START khi Camera không trong quá trình kiểm tra kết nối cam.
                    Nếu START thất bại => Buộc kết thúc bài thi.
                Nếu trả về STOP: Kết thúc bài thi.
                Nếu trả về ALLSTOP: Kết thúc tất cả bài thi. Xóa tên và xóa mã thi sinh. 
        """
        self.rs232_state = self.rs232.run()
        if self.rs232_state == START:
            if self.is_camchecking:
                stated = False
            else:
                stated = self.StartBaiThi(self.rs232.thisinh.BaiThi, self.rs232.thisinh.ten, self.rs232.thisinh.maTS)
            
            if stated:
                self.rs232.thisinh.Thi = STARTED
                baithi_log.info("RS232: TRANSMIT: " + self.rs232.thisinh.BaiThi + READY)
                self.rs232.SerialWriteStr(self.rs232.thisinh.BaiThi + READY)
            else:
                self.rs232_state = STOP
                self.rs232.thisinh.Thi = STOP
                baithi_log.info("RS232: TRANSMIT: " + self.rs232.thisinh.BaiThi + ERROR)
                self.rs232.SerialWriteStr(self.rs232.thisinh.BaiThi + ERROR)
            pass
        elif self.rs232_state == STOP:
            if self.StopBaiThi():
                if self.rs232.thisinh.Thi == ERROR:
                    baithi_log.info("RS232: TRANSMIT: " + self.rs232.thisinh.BaiThi + ERROR)
                    self.rs232.SerialWriteStr(self.rs232.thisinh.BaiThi + ERROR)
                else:
                    baithi_log.info("RS232: TRANSMIT: " + self.rs232.thisinh.BaiThi + STOPOK)
                    self.rs232.SerialWriteStr(self.rs232.thisinh.BaiThi + STOPOK)
                self.rs232.thisinh.Thi = STOPED      
        elif self.rs232_state == ALLSTOP:
            if self.StopBaiThi():
                self.rs232.thisinh.Thi = STOPED
                baithi_log.info("RS232: TRANSMIT: " + self.rs232.thisinh.ten + ":" + self.rs232.thisinh.maTS + ":" + ALLSTOP)
                self.rs232.SerialWriteStr(self.rs232.thisinh.ten + ":" + self.rs232.thisinh.maTS + ":" + ALLSTOP)
                self.rs232.thisinh.ten  = ""
                self.rs232.thisinh.maTS =""
                self.data = ""
        pass
       
    def layout_run(self):
        """
            Chương trình xử lý cho giao tiếp GIAO DIỆN với chương trình:
            Kiểm tra cờ data:
                Nếu trả về START: Khởi động bài thi tương ứng.
                    Cho phép START khi Camera không trong quá trình kiểm tra kết nối cam.
                    Nếu START thất bại => Buộc kết thúc bài thi.
                Nếu trả về STOP: Kết thúc bài thi.
                Nếu trả về ALLSTOP: Kết thúc tất cả bài thi. Xóa tên và xóa mã thi sinh. 
        """
        if self.data == START:
            stated = self.StartBaiThi(self.rs232.thisinh.BaiThi, self.rs232.thisinh.ten, self.rs232.thisinh.maTS)
            if stated:
                self.data = STARTED
                # try:
                self.rs232.thisinh.Thi = STARTED
                baithi_log.info("RS232: TRANSMIT: " + self.rs232.thisinh.BaiThi + READY)
                self.rs232.SerialWriteStr(self.rs232.thisinh.BaiThi + READY)
                # except:
                #     root_log.error("LOI TRONG QUA TRINH GUI DU LIEU: \n" + str(traceback.format_exc()))
                #     pass
            else:
                self.data = STOP
                self.rs232.thisinh.Thi = ERROR
            pass
        elif self.data == STOP:
            if self.StopBaiThi():
                self.data = ""
                # try:
                if self.rs232.thisinh.Thi == ERROR:
                    baithi_log.info("RS232: TRANSMIT: " + self.rs232.thisinh.BaiThi + ERROR)
                    self.rs232.SerialWriteStr(self.rs232.thisinh.BaiThi + ERROR)
                else:
                    baithi_log.info("RS232: TRANSMIT: " + self.rs232.thisinh.BaiThi + STOPOK)
                    self.rs232.SerialWriteStr(self.rs232.thisinh.BaiThi + STOPOK)
                # except:
                #     root_log.error("LOI TRONG QUA TRINH GUI DU LIEU:\n" + str(traceback.format_exc()))
                #     pass
                self.rs232.thisinh.Thi = STOPED
   
        pass
    
    def run(self):
        """
            run:
            Gọi quá trình xử lý giao tiếp với Giao diện
            Gọi quá trình xử lý giao tiếp RS232
            Kiểm tra kết quả xử lý ảnh (Nếu bài thi STARTED)
            Kiểm tra kết nối cam trong quá trình thi.
        """
        self.layout_run()
        try:
            self.rs232_run()
        except:
            root_log.error("LOI KHI CHAY RS232:\n" + str(traceback.format_exc()))
            pass
        if self.rs232.thisinh.Thi == STARTED:
            self.KiemTraKetQua(self.rs232.thisinh.BaiThi)
            self.KiemTraLoiCam()

    # Buộc dừng chương trình. Hàm trung gian giúp Giao Diện gọi STOP bài thi trực tiếp.
    def force_stop(self):
        self.StopBaiThi()
