import os
from setting import *
from datetime import date
import shutil
from log import root_log

def try_create_folder_result(mathisinh, result_folder):
    resutl = create_folder(mathisinh, result_folder)
    return resutl[1]

def create_folder_result(mathisinh, result_folder):
    result = create_folder(mathisinh, result_folder)
    return result[0]

def create_folder(mathisinh, result_folder):
    path = result_folder
    todays_date = date.today()
    list_bai_thi = [HANGDINHFOLDER, SO8FOLDER, QUAYVONGFOLDER, ZICZACFOLDER]
    list_camerea = [NAME_CAM_1, NAME_CAM_2, NAME_CAM_5, NAME_CAM_6, NAME_CAM_3, NAME_CAM_4]

    day = str(todays_date.day)
    month = str(todays_date.month)
    year = str(todays_date.year)
    full_time = day + month + year

    countbuff = 1
    maTS = mathisinh
    if(mathisinh == ""):
        ten_thi_sinh = "TS" + str(countbuff) + "_" + full_time
        maTS = "TS" + str(countbuff)
        root_log.warning("MAKEDIR: Ma TS rong! Tu dong tao ma TS: ")
    else:
        ten_thi_sinh = maTS + '_' + full_time
        
    while True:
        try:
            os.makedirs(path + ten_thi_sinh)
            root_log.info("MAKEDIR: FOLDER Thi sinh: " + ten_thi_sinh)
            break
        except:
            countbuff += 1
            if(mathisinh != ""):
                break
            else:
                ten_thi_sinh = "TS" + str(countbuff) + "_" + full_time
                maTS = "TS" + str(countbuff)
    try:
        for fol in list_bai_thi:
            if fol == HANGDINHFOLDER:
                os.makedirs(path + ten_thi_sinh + DEVACHFOLDER      + fol + "\\" + list_camerea[4])
                os.makedirs(path + ten_thi_sinh + VIDEORECORDFOLDER + fol + "\\" + list_camerea[4])
                os.makedirs(path + ten_thi_sinh + DEVACHFOLDER      + fol + "\\" + list_camerea[5])
                os.makedirs(path + ten_thi_sinh + VIDEORECORDFOLDER + fol + "\\" + list_camerea[5])
            else:
                for i in range(0, 4):
                    os.makedirs(path + ten_thi_sinh + DEVACHFOLDER      + fol + "\\" + list_camerea[i])
                    os.makedirs(path + ten_thi_sinh + VIDEORECORDFOLDER + fol + "\\" + list_camerea[i])
    except:
        # print(error)
        pass

    pass_result = [path + ten_thi_sinh, maTS]
    return pass_result

def Creat_Log_Folder():
    path = DEBUG_FOLDER
    countbuff = 1
    while True:
        try:
            os.makedirs(path + str(countbuff))
            pass_result = path + str(countbuff)
            try:
                if countbuff == LOGSAVEMAX:
                    countbuff = 0
                shutil.rmtree(path + str(countbuff + 1))
            except:
                pass
            break
        except:
            if countbuff == LOGSAVEMAX:
                countbuff = 0
                shutil.rmtree(path + str(1))
            countbuff += 1

    return pass_result

    
