from log import *
import sys
import glob
import time
import serial
from setting import *



class ThiSinh:
    ten  = ""
    maTS = ""
    serialHandle = serial.Serial()
    tick = 0.0
    BaiThi = ""
    Thi = STOPED
    Loi = ""

def SerialComList():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def SerialOpen(port):
    serialHandle = serial.Serial()
    serialHandle.parity = PARITY
    serialHandle.bytesize = BYTESIZE
    serialHandle.stopbits = STOPBITS
    serialHandle.baudrate = BAUDRATE
    serialHandle.port = port
    serialHandle.open()
    return serialHandle

def SerialWriteStr(serialHandle, data):
    data = (data + "\n").encode()
    try:
        serialHandle.write(data)
    except:
        root_log.error("RS232: LOI COM:\n" + str(traceback.format_exc()))
        #print("LOI COM!")
        pass

def SerialReadLine(serialHandle):
    data = ""
    b = serialHandle.in_waiting
    while b > 0:
        buff = serialHandle.read()
        time.sleep(0.01)
        if not buff == b'\n':
            # Xử lý kí tự đặc biết (có mã < 47). Cho phép nhận kí tự "space" (mã 32)
            if ord(buff) > 47 or ord(buff) == 32: 
                data += buff.decode()
            elif ord(buff):
                 # In ra mã kí tự đặc biệt nhận về.
                 # Đọc hết nội dung lỗi nhận về
                 # Báo lỗi nhận kí tự
                data = "Ki tu dac biet:" + str(ord(buff))
                while b > 0: 
                    buff = serialHandle.read()
                    time.sleep(0.01)
                    b = serialHandle.in_waiting
                return data
        else:
            # Trả về chuỗi khi nhận được kí tự Enter
            return data
        b = serialHandle.in_waiting
    if data != "":
        # Trả về chuỗi NONE nếu chuỗi nhận về không có kí tự ENTER
        data = "NONE"
    return data

def CommandCheck(data):
    for command in COMMAND:
        if Find(data, command):
            return 1
    return 0

class RS232:
    def __init__(self):
        self.thisinh = ThiSinh
        self.thisinh.BaiThi = ALLSTOP
        self.is_ready = False
        self.camcheck = False

    def SerialOpen(self, port):
        self.thisinh.serialHandle.parity = PARITY_NONE
        self.thisinh.serialHandle.bytesize = EIGHTBITS
        self.thisinh.serialHandle.stopbits = STOPBITS_ONE
        self.thisinh.serialHandle.baudrate = 9600
        self.thisinh.serialHandle.port = port
        self.thisinh.serialHandle.open()
        return self.thisinh.serialHandle

    def Open(self, port='COM1'):
        try:
            self.SerialOpen(port)
            self.is_ready = True
            # print("Mo:" + port)
            self.SerialWriteStr("START")
            root_log.info("Mo: " + port)
            root_log.info("START:")
        except:
            root_log.warning("Khong the mo doi tuong: " + port)
            # print("Khong the mo doi tuong: ", port)
            pass

    def SerialWriteStr(self, data):
        result = False
        if self.is_ready:
            try:
                data = (data + "\n").encode()
                self.thisinh.serialHandle.write(data)
                result = True
            except:
                self.is_ready = False
                root_log.error("LOI TRONG QUA TRINH GUI DU LIEU:\n" + str(traceback.format_exc()))
        return result
        
    
    def KhoiDong(self, data, serialHandle):
        if Find(data, KDCONNECT):
            root_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + KDCONNECTED)
            SerialWriteStr(serialHandle, KDCONNECTED)
        elif Find(data, KDCAMCHECK):
            root_log.info("RS232: RECIEVER: " + data)
            self.camcheck = True

    def DotThiMoi(self, data):
        # Kiem tra Bat dau dot thi moi
        # Neu san sang phan hoi OK, neu chua phan hoi ERROR
        # Nhan du lieu ten thi sinh, phan hoi Ten thi sinh + OK neu da nhan, phan hoi ERROR neu k nhan duoc sau 5s hoac truyen sai
        result = "NONE"
        if (Find(data, NEWSTART)):
            # Code cu
            # sansang = 1
            # if (sansang == 1):
            # Ket thuc code cu
            if self.thisinh.BaiThi == ALLSTOP:
                baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + NEWSTART + ":" + NEWOK)
                SerialWriteStr(self.thisinh.serialHandle, NEWOK)
            else:
                baithi_log.warning("Bai thi chua ket thuc, command khong the thuc hien: " + data)
                baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + NEWERROR)
                SerialWriteStr(self.thisinh.serialHandle, NEWERROR)
            result = NEWSTART
        elif (Find(data, NEWTHISINH)):
            baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + NEWTHISINHOK)
            SerialWriteStr(self.thisinh.serialHandle, NEWTHISINHOK)
            tick = time.time()
            while (tick + TIME_ERROR > time.time()):
                data = SerialReadLine(self.thisinh.serialHandle)
                data = data.split("\n")[0] # Bo dau enter cuoi chuoi nhan ve
                if (data != ""):
                    if (Find(data, NEW + ":")):
                        try:
                            self.thisinh.ten = data.split(":", 2)[1]  # Tach lay ten thi sinh la chuoi thu 2, ngay sau dau ":"
                            self.thisinh.maTS = data.split(":", 2)[2]   # Tach lay ma thi sinh
                            SerialWriteStr(self.thisinh.serialHandle, (NEW + ":" + self.thisinh.ten + ":" + self.thisinh.maTS + "\n" + OK))
                            baithi_log.info("Da nhan du lieu thi sinh:")
                            baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + NEW + ":" + self.thisinh.ten + ":" + self.thisinh.maTS + ": " + OK)
                            result = NEWTEN
                            break
                        except:
                            baithi_log.warning("Loi du lieu thi sinh: ")
                            baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + NEWERROR)
                            SerialWriteStr(self.thisinh.serialHandle, NEWERROR)
                            result = NEWERROR
                            break
                    elif not data == 'NONE':
                        break
            if result == "NONE":
                baithi_log.warning("Qua thoi gian Timeout nhan du lieu, nhan du lieu thi sinh that bai:")
                baithi_log.info("RS232: RECIEVER: " + result + " TRANSMIT: " + NEWERROR)
                SerialWriteStr(self.thisinh.serialHandle, NEWERROR)
                result = NEWERROR
        else:
            pass
        return result

    def NhanThiSinh(self, data):
        if(Find(data, KD)):
            self.KhoiDong(data, self.thisinh.serialHandle)
        elif (Find(data, NEW)):
            result = self.DotThiMoi(data)
            if result == NEWTEN:
                self.thisinh.BaiThi = NEWOK

    def isStart_Baithi(self, data):
        list_baithi = [BAITHISO8, BAITHIZICZAC, BAITHIHANGDINH, BAITHIQUAYVONG]
        for baithi in list_baithi:
            if Find(data, baithi + START):
                if self.thisinh.BaiThi != ALLSTOP:
                    if self.thisinh.Thi == STOPED:
                        self.thisinh.BaiThi = baithi
                        self.thisinh.Thi = START
                        self.thisinh.Loi = ""
                        baithi_log.info("RS232: RECIEVER: " + self.thisinh.BaiThi + self.thisinh.Thi)
                        return 1
                    else:
                        if self.thisinh.BaiThi == baithi:
                            baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + baithi + OK)
                            self.SerialWriteStr(baithi + OK)
                            return 1
                        else:
                            baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + baithi + ERROR)
                            self.SerialWriteStr(baithi + ERROR)
                            return 1
                else:
                    baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + baithi + ERROR)
                    self.SerialWriteStr(baithi + ERROR)
                    return 1
        return 0

    def isStop_Baithi(self, data):
        list_baithi = [BAITHISO8, BAITHIZICZAC, BAITHIHANGDINH, BAITHIQUAYVONG]
        for baithi in list_baithi:
            if Find(data, baithi + STOP):
                self.thisinh.BaiThi = baithi
                self.thisinh.Thi = STOP
                self.thisinh.Loi = ""
                baithi_log.info("RS232: RECIEVER: " + self.thisinh.BaiThi + self.thisinh.Thi)

    def SerialReply(self, str):
        if (self.thisinh.tick + 5 < time.time()):
            self.thisinh.tick = time.time()
            baithi_log.info("RS232: REPLY: " + str)
            self.SerialWriteStr(str)

    def run(self):
        result = "NONE"
        if self.is_ready:
            self.running()
            result = self.thisinh.Thi
        return result

    def running(self):
        data = SerialReadLine(self.thisinh.serialHandle)
        if data == "":
            #Phản hồi NEWOK liên tục duy trì 5s sau khi nhận thông tin thí sinh nếu chưa start bài thi.
            if self.thisinh.BaiThi == NEWOK:
                self.SerialReply(NEWOK)
            return 0
        if not CommandCheck(data):
            baithi_log.warning("Loi command: " + data)
            baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + ERROR)
            SerialWriteStr(self.thisinh.serialHandle, ERROR)
            return 0
        #Kết thúc toàn bộ quá trình thi.

        if Find(data, SHUTDOWN):
            self.SerialWriteStr(SHUTDOWN + OK)
            time.sleep(0.1)
            os.system("shutdown -s -t 1")
        if Find(data, RESTART):
            self.SerialWriteStr(RESTART + OK)
            time.sleep(0.1)
            os.system("shutdown -r -t 1")
        # if Find(data, CANCLE):
        #     self.SerialWriteStr(CANCLE + OK)
        #     os.system("shutdown - a")

        if Find(data, ALLSTOP):
            baithi_log.info("RS232: RECIEVER: " + data)
            self.thisinh.Thi = ALLSTOP
            self.thisinh.BaiThi = ALLSTOP
            return 0

        #Nhận vào thông tin của thí sinh.
        #Báo lỗi khi start không có thông tin thí sinh!
        if self.thisinh.BaiThi == ALLSTOP:
            self.NhanThiSinh(data)
            self.isStart_Baithi(data)
            pass
        #Cho phép gửi lại thông tin của thí sinh.
        #Nhận lệnh Start bài thi hoặc báo lỗi nếu câu lệnh start sai.
        elif self.thisinh.BaiThi == NEWOK:
            self.NhanThiSinh(data)
            self.isStart_Baithi(data)
            # self.SerialReply(NEWOK)
            pass
        #Chạy bài thi và báo lỗi lệnh command nhập sai.
        #Kết thúc bài thi và cho phép start lại bài thi tiếp theo.
        else:
            if self.thisinh.Thi == STARTED:
                if Find(data, self.thisinh.BaiThi + STOP):
                    baithi_log.info("RS232: RECIEVER: " + data)
                    self.thisinh.Thi = STOP
                else:
                    # Khi bài thi đang diễn ra, gửi sai lệnh command sẽ báo lỗi!
                    baithi_log.warning("Bai thi: \"" + self.thisinh.BaiThi + "\" dang chay. Loi command: " + data)
                    if not self.isStart_Baithi(data):
                        if Find(data, NEW):
                            baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + NEWERROR)
                            SerialWriteStr(self.thisinh.serialHandle, NEWERROR)
                        else:
                            baithi_log.info("RS232: RECIEVER: " + data + " TRANSMIT: " + ERROR)
                            SerialWriteStr(self.thisinh.serialHandle, ERROR)
            elif self.thisinh.Thi == STOPED:
                self.NhanThiSinh(data)
                self.isStart_Baithi(data)
            pass
