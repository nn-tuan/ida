import torch
import cv2
import segmentation_models_pytorch as smp
import albumentations as albu
import numpy as np
import math
import time
from log import *
import matplotlib.pyplot as plt
from Classify import Classify
import random, string

print("is_available : ", torch.cuda.is_available())

def to_tensor(x, **kwargs):
    return x.transpose(2, 0, 1).astype('float32')

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform

    Args:
        preprocessing_fn (callbale): data normalization function
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose

    """

    _transform = [
        albu.Lambda(image=preprocessing_fn),
        albu.Lambda(image=to_tensor),
    ]
    return albu.Compose(_transform)

def preprocessing_fn_fun():
    ENCODER = 'resnet50'
    ENCODER_WEIGHTS = 'imagenet'
    preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)
    return preprocessing_fn

class Segmemtation(SETING):
    def __init__(self) -> None:
        SETING.__init__(self)
        self.preprocessing_fn = preprocessing_fn_fun()
        self.DEVICE = 'cuda'

        self.banh_xe = torch.load('./data/best_model_banh.pth')
        self.vach = torch.load('./data/best_model_vach.pth')
        self.classify = Classify("./data/model.pt")
        self.setting_update()
        """
            Nếu chạy bằng CPU:
                self.DEVICE = 'cpu'
                self.banh_xe = torch.load('./data/best_model_banh_xe.pth', map_location='cpu')
                self.vach = torch.load('./data/best_model_vach_r.pth', map_location='cpu')
        """
        self.index = 0

    def chenvach(self, mask1, mask2):
        contours1, hierarchy = cv2.findContours(mask1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        contours2, hierarchy = cv2.findContours(mask2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        if len(contours1) == 0 or len(contours2) == 0:
            return 0, 0, 0
        contours2 = sorted(contours2, key=cv2.contourArea)
        contours1 = sorted(contours1, key=cv2.contourArea)
        lop = np.zeros(mask2.shape, np.uint8)
        vach = np.zeros(mask1.shape, np.uint8)
        # cv2.imwrite('lop.jpg',mask2)
        # cv2.imwrite('vach.jpg',mask1)
        # cv2.drawContours(lop, [contours2[-1]], -1, 255, cv2.FILLED, 1)
        if len(contours2) == 1:
            cv2.drawContours(lop, contours2, -1, 255, cv2.FILLED, 1)
        else:
            contours_lop= []
            for i in range(0, 2):
                if cv2.contourArea(contours2[i]) > 20:
                    contours_lop.append(contours2[i])
                    cv2.drawContours(lop, contours_lop, -1, 255, cv2.FILLED, 1)
        if len(contours1) == 1:
            cv2.drawContours(vach, contours1, -1, 255, cv2.FILLED, 1)
        else:
            contours_vach = []
            for i in range(0, 2):
                if cv2.contourArea(contours1[i]) > 20:
                    contours_vach.append(contours1[i])
                    cv2.drawContours(vach, contours_vach, -1, 255, cv2.FILLED, 1)
        kernel = np.ones((2,2),np.uint8)
        # lop = lop[round(start_point[1]/4.57):round(end_point[1]/4.57),round(start_point[0]/4.57):round(end_point[0]/4.57)] # 4.57 =512/112 size của ảnh gốc và ảnh output
        # vach = vach[round(start_point[1]/4.57):round(end_point[1]/4.57),round(start_point[0]/4.57):round(end_point[0]/4.57)]
        lop = cv2.dilate(lop,kernel,iterations = 1)
        vach = cv2.dilate(vach,kernel,iterations = 1)
        h,w = lop.shape
        pos_x=0
        pos_y=0
        sum=0
        intersection1 = np.logical_and(lop,vach)
        for i in range(0,h):
            for j in range (0,w):
                if intersection1[i,j]== True:
                    pos_x=pos_x+i
                    pos_y=pos_y+j
                    sum=sum+1
        if sum > 0:
            pos_x=round (pos_x/sum)
            pos_y=round(pos_y/sum)

        intersection = np.sum(np.logical_and(lop,vach))
        
        return intersection, pos_x, pos_y

    # def chenvach(self, mask1, mask2, nameCam="None"):
    #     contours1, hierarchy = cv2.findContours(mask1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    #     contours2, hierarchy = cv2.findContours(mask2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    #     if len(contours1) == 0 or len(contours2) == 0:
    #         return 0
    #     contours2 = sorted(contours2, key=cv2.contourArea)
    #     contours1 = sorted(contours1, key=cv2.contourArea)
    #     lop = np.zeros(mask2.shape, np.uint8)
    #     vach = np.zeros(mask1.shape, np.uint8)
    #     if len(contours2) == 1:
    #         cv2.drawContours(lop, contours2, -1, 255, cv2.FILLED, 1)
    #     else:
    #         contours_lop= []
    #         for i in range(0, 2):
    #             if cv2.contourArea(contours2[i]) > 30:
    #                 contours_lop.append(contours2[i])
    #                 cv2.drawContours(lop, contours_lop, -1, 255, cv2.FILLED, 1)
    #     if len(contours1) == 1:
    #         cv2.drawContours(vach, contours1, -1, 255, cv2.FILLED, 1)
    #     else:
    #         contours_vach = []
    #         for i in range(0, 2):
    #             if cv2.contourArea(contours1[i]) > 30:
    #                 contours_vach.append(contours1[i])
    #                 cv2.drawContours(vach, contours_vach, -1, 255, cv2.FILLED, 1)
                    
    #     kernel = np.ones((2, 2),np.uint8)
    #     if nameCam == NAME_CAM_1:
    #         kernel = np.ones((2, 2),np.uint8)
    #     elif nameCam == NAME_CAM_2:
    #         kernel = np.ones((2, 2),np.uint8)
    #     elif nameCam == NAME_CAM_3:
    #         kernel = np.ones((5, 5),np.uint8)
    #     elif nameCam == NAME_CAM_4:
    #         kernel = np.ones((5, 5),np.uint8)
    #     elif nameCam == NAME_CAM_5:
    #         kernel = np.ones((5, 5),np.uint8)
    #     elif nameCam == NAME_CAM_6:
    #         kernel = np.ones((5, 5),np.uint8)
    #     else:
    #         kernel = np.ones((3, 3),np.uint8)
    #         tes1 = cv2.dilate(mask1, kernel, iterations = 1)
    #         tes2 = cv2.dilate(mask2, kernel, iterations = 1)
    #         cv2.imwrite("testLop.jpg", tes1)
    #         cv2.imwrite("testvach.jpg", tes2)
    #         intersection = np.sum(np.logical_and(tes1,tes2))
    #         return intersection

    #     lop = cv2.dilate(lop, kernel, iterations = 1)
    #     vach = cv2.dilate(vach, kernel, iterations = 1)
    #     intersection = np.sum(np.logical_and(lop,vach))
    #     return intersection
    
    def run(self, image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = cv2.resize(image, (320, 320))
        sample = get_preprocessing(self.preprocessing_fn)(image=image)
        img = sample["image"]
        x_tensor = torch.from_numpy(img).to(self.DEVICE).unsqueeze(0)
        pr_mask_banh_xe = self.banh_xe.predict(x_tensor)
        pr_mask_banh_xe = (pr_mask_banh_xe.squeeze().cpu().numpy())
        pr_mask_vach = self.vach.predict(x_tensor)
        pr_mask_vach = (pr_mask_vach.squeeze().cpu().numpy())
        return pr_mask_banh_xe * 255, pr_mask_vach * 255

    def get_rect(self, ROI_CHENVACH):
        if (len(ROI_CHENVACH)==4):

            delta_x_segment = self.roi_segment[2] - self.roi_segment[0]
            delta_y_segment = self.roi_segment[3] - self.roi_segment[1]
            delta_x_chenvach =ROI_CHENVACH[2]-ROI_CHENVACH[0]
            delta_y_chenvach =ROI_CHENVACH[3]-ROI_CHENVACH[1] 

            ROI_CHENVACH[0]=ROI_CHENVACH[0] - self.roi_segment[0]
            ROI_CHENVACH[1]=ROI_CHENVACH[1] - self.roi_segment[1]
            ROI_CHENVACH[2]=ROI_CHENVACH[0]+ delta_x_chenvach
            ROI_CHENVACH[3]=ROI_CHENVACH[1]+ delta_y_chenvach

            ROI_CHENVACH[0]=math.floor(ROI_CHENVACH[0]*320/delta_x_segment)
            ROI_CHENVACH[2]=math.floor(ROI_CHENVACH[2]*320/delta_x_segment)
            ROI_CHENVACH[1]=math.floor(ROI_CHENVACH[1]*320/delta_y_segment)
            ROI_CHENVACH[3]=math.floor(ROI_CHENVACH[3]*320/delta_y_segment)
        else:
            ROI_CHENVACH[0]=ROI_CHENVACH[0]-self.roi_segment[0]
            ROI_CHENVACH[1]=ROI_CHENVACH[1]-self.roi_segment[1]
            ROI_CHENVACH[2]=ROI_CHENVACH[2]-self.roi_segment[0]
            ROI_CHENVACH[3]=ROI_CHENVACH[3]-self.roi_segment[1]
            ROI_CHENVACH[4]=ROI_CHENVACH[4]-self.roi_segment[0]
            ROI_CHENVACH[5]=ROI_CHENVACH[5]-self.roi_segment[1]
            ROI_CHENVACH[6]=ROI_CHENVACH[6]-self.roi_segment[0]
            ROI_CHENVACH[7]=ROI_CHENVACH[7]-self.roi_segment[1]
            delta_x_segment = self.roi_segment[2]-self.roi_segment[0]
            delta_y_segment = self.roi_segment[3]-self.roi_segment[1]
            ROI_CHENVACH[0]=math.floor(ROI_CHENVACH[0]*320/delta_x_segment)
            ROI_CHENVACH[2]=math.floor(ROI_CHENVACH[2]*320/delta_x_segment)
            ROI_CHENVACH[4]=math.floor(ROI_CHENVACH[4]*320/delta_x_segment)
            ROI_CHENVACH[6]=math.floor(ROI_CHENVACH[6]*320/delta_x_segment)
            ROI_CHENVACH[1]=math.floor(ROI_CHENVACH[1]*320/delta_y_segment)
            ROI_CHENVACH[3]=math.floor(ROI_CHENVACH[3]*320/delta_y_segment)
            ROI_CHENVACH[5]=math.floor(ROI_CHENVACH[5]*320/delta_y_segment)
            ROI_CHENVACH[7]=math.floor(ROI_CHENVACH[7]*320/delta_y_segment)

        return ROI_CHENVACH
        
    def __call__(self, image, roiCam, saveFolder, nameCam, count, baithi, ten, maTS):
        # cv2.imshow(nameCam, image)
        # cv2.waitKey(1)
        roi = roiCam.copy()
        ROI_CHENVACH = self.get_rect(roi)
        img_save = image.copy()
        image = image[self.roi_segment[1]: self.roi_segment[3], self.roi_segment[0]: self.roi_segment[2]]
        img_crop_box = image.copy()

        img_copy = image.copy()
        img_save_test = img_copy.copy()
        
        img_copy = cv2.resize(img_copy, (320, 320))
        img_dedinh = img_copy.copy()

        img_copy = cv2.cvtColor(img_copy, cv2.COLOR_BGR2RGB)
        img_vach_save = img_copy.copy()
        
        sample = get_preprocessing(self.preprocessing_fn)(image=img_copy)
        img = sample["image"]
        x_tensor = torch.from_numpy(img).to(self.DEVICE).unsqueeze(0)
        pr_mask_banh_xe = self.banh_xe.predict(x_tensor)
        mask_banh_xe = (pr_mask_banh_xe.squeeze().cpu().numpy()) * 255.0
        pr_mask_vach = self.vach.predict(x_tensor)      
        mask_vach = (pr_mask_vach.squeeze().cpu().numpy()) * 255.0
        
        vis_vach = np.zeros(shape=(320, 320), dtype=np.uint8)
        vis_banhxe = np.zeros(shape=(320, 320), dtype=np.uint8)

        vis = np.zeros(shape=(320, 320, 3), dtype=np.uint8)

        vis_banhxe[mask_banh_xe > 150] = 255
        # vis_banhxe[mask_vach > 150] = 0
        vis_vach[mask_vach > 150] = 255
        vis_vach[mask_banh_xe > 150] = 0

        vis[mask_banh_xe > 150] = (123, 56, 89)
        vis[mask_vach > 150] = (56, 200, 189)

        img_vach_save[mask_vach < 150] = (0, 0, 0)

        # cv2.imwrite("img_vach_save.jpg", img_vach_save)

        

        if (len(ROI_CHENVACH)==4):
            vis_banhxe_1=vis_banhxe[ROI_CHENVACH[1] : ROI_CHENVACH[3], ROI_CHENVACH[0] : ROI_CHENVACH[2]]
            vis_vach_1=vis_vach[ROI_CHENVACH[1] : ROI_CHENVACH[3], ROI_CHENVACH[0] : ROI_CHENVACH[2]]
            
        else:
            points = np.array([[[ROI_CHENVACH[0],ROI_CHENVACH[1]],[ROI_CHENVACH[2],ROI_CHENVACH[3]],[ROI_CHENVACH[4],ROI_CHENVACH[5]],[ROI_CHENVACH[6],ROI_CHENVACH[7]]]])
            mask= np.zeros(shape=(320, 320), dtype=np.uint8)
            cv2.drawContours(mask, [points], -1, (255, 255, 255), -1, cv2.LINE_AA)
            vis_banhxe_1= cv2.bitwise_and(vis_banhxe, mask)
            vis_vach_1= cv2.bitwise_and(vis_vach, mask)

        # vis_banhxe_1 = vis_banhxe[ROI_CHENVACH[1]: ROI_CHENVACH[3], ROI_CHENVACH[0]: ROI_CHENVACH[2]]
        # vis_vach_1 = vis_vach[ROI_CHENVACH[1]: ROI_CHENVACH[3], ROI_CHENVACH[0]: ROI_CHENVACH[2]]

        check, pos_x, pos_y = self.chenvach(vis_vach_1, vis_banhxe_1) # self.chenvach(vis_vach_1, vis_banhxe_1, nameCam)

        print(check, pos_x, pos_y)

        if check > 0:
            pos_x += ROI_CHENVACH[0]
            pos_y += ROI_CHENVACH[1]

            x = 0 if pos_x < 50 else pos_x - 50
            y = 0 if pos_y < 50 else pos_y - 50
            
            w = 100 if x + 100 < img_crop_box.shape[1] else img_crop_box.shape[1] - 100
            h = 100 if y + 100 < img_crop_box.shape[0] else img_crop_box.shape[0] - 100

            print(img_crop_box.shape)

            img_crop_box = cv2.resize(img_crop_box, (320, 320))

            ing_classify = img_crop_box[y: y + h, x: x + w]

            if not os.path.isdir("de"):
                os.makedirs("de")

            if not os.path.isdir("ko"):
                os.makedirs("ko")

            label = self.classify(ing_classify.copy())

            name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))

            if label != "de":
                cv2.imwrite("ko/" + str(name) + ".jpg", ing_classify)
                check = 0
            else:
                cv2.imwrite("de/" + str(name) + ".jpg", ing_classify)

        if not os.path.isdir(saveFolder + "/{}".format(nameCam)):
            os.makedirs(saveFolder + "/{}".format(nameCam))

        if not os.path.isdir(saveFolder + "/{}".format("AnhSeg")):
            os.makedirs(saveFolder + "/{}".format("AnhSeg"))

        # cv2.rectangle(img_save_test, (ROI_CHENVACH[0], ROI_CHENVACH[1]), (ROI_CHENVACH[2], ROI_CHENVACH[3]), (255, 34, 70), 2)
        cv2.imwrite(saveFolder + "/{2}/{0}_img_seg_{1}".format(self.index, check, nameCam) + ".jpg", vis)
        cv2.imwrite(saveFolder + "/{2}/{0}_img_{1}".format(self.index, check, nameCam) + ".jpg", img_save_test)
        # cv2.imwrite("./myLib/ketquatest/seg_banhxe_1" + str(self.index) + ".jpg", vis_banhxe_1)
        # cv2.imwrite("./myLib/ketquatest/seg_vach" + str(self.index) + ".jpg", vis_vach)
        # cv2.imwrite("./myLib/ketquatest/seg_vach_crop" + str(self.index) + ".jpg", vis_vach_1)
        self.index += 1

        '''
        print("0000000000000000000000")

        cv2.imwrite("vis_banhxe_1.jpg", vis_banhxe_1)
        cv2.imwrite("vis_vach_1.jpg", vis_vach_1)
        cv2.imwrite("vis_vach.jpg", vis_vach)
        cv2.imwrite("img_save.jpg", img_save)

        print("11111111111111111111111")70
        '''
        # cv2.imshow
        
        check_dedinh = 0
        try:
            if nameCam == NAME_CAM_3 or nameCam == NAME_CAM_4:
                hsv = cv2.cvtColor(img_dedinh, cv2.COLOR_BGR2HSV)
                # define range of red color in HSV
                lower_red1 = np.array([0, 80, 80])
                upper_red1 = np.array([30, 255, 255])
                
                lower_red2 = np.array([150, 80, 80])
                upper_red2 = np.array([180, 255, 255])
                
                # Threshold the HSV image to get only red colors
                mask1 = cv2.inRange(hsv, lower_red1, upper_red1)
                mask2 = cv2.inRange(hsv, lower_red2, upper_red2)
                
                mask = cv2.bitwise_or(mask1, mask2)
                
                # Bitwise-AND mask and original image
                res = cv2.bitwise_and(img_dedinh, img_dedinh, mask=mask)
                kernel = np.ones((36, 36),np.uint8)
                mask = cv2.dilate(mask2, kernel, iterations = 1)
                mask = mask[ROI_CHENVACH[1] : ROI_CHENVACH[3], ROI_CHENVACH[0] : ROI_CHENVACH[2]]
                cv2.imwrite('Anh.jpg', img_dedinh)
                cv2.imwrite("result.jpg", res)
                cv2.imwrite("Vach.jpg", vis_vach)
                check_dedinh = self.chenvach(vis_banhxe_1, mask, "None")
                if check_dedinh != 0:
                    cv2.imwrite("DEDINH.jpg", img_dedinh)
        except:
            pass
        # if check <= 3:
        if check == 0 and check_dedinh == 0:
            #print("Khong de vach:", nameCam)
            return 0
        else:
            
            # Thêm thông tin thí sinh, thời gian vào ảnh:
            time_string = time.strftime("%d/%m/%Y, %H:%M:%S", time.localtime())
            img_save = cv2.putText( img_save, 
                                    time_string, 
                                    (10, 30),
                                    cv2.FONT_HERSHEY_SIMPLEX, 
                                    0.75, 
                                    (0, 255, 0), 
                                    2, 
                                    cv2.LINE_AA)

            img_save = cv2.putText( img_save, 
                                    maTS + ":" + ten, 
                                    (10, 60),
                                    cv2.FONT_HERSHEY_SIMPLEX, 
                                    0.75, 
                                    (0, 255, 0), 
                                    2, 
                                    cv2.LINE_AA)

            img_save = cv2.putText( img_save , 
                                    "Bai thi:" + " " + baithi, 
                                    (10, 90),
                                    cv2.FONT_HERSHEY_SIMPLEX, 
                                    0.75, 
                                    (0, 255, 0), 
                                    2, 
                                    cv2.LINE_AA)

            # Vẽ vùng ROI lên ảnh:
            img_save = cv2.rectangle(img_save, (self.roi_segment[0], self.roi_segment[1]), (self.roi_segment[2], self.roi_segment[3]), (255, 0, 255), 4)
            
            #img_save = cv2.rectangle(img_save, (roiCam[0], roiCam[1]), (roiCam[2], roiCam[3]), (0, 0, 255), 4)
            #comment vẽ vùng ROI tứ giác:
            if len(roiCam) != 4:
                img_save = self.draw_poly(image=img_save,
                          pts= [(roiCam[0], roiCam[1]), (roiCam[2], roiCam[3]), (roiCam[4], roiCam[5]), (roiCam[6], roiCam[7])],
                          is_closed=True,
                          color_bgr=[0, 0, 255], 
                          size= 0.01, 
                          is_copy=False)
            else:
                img_save = cv2.rectangle(img_save, (roiCam[0], roiCam[1]), (roiCam[2], roiCam[3]), (0, 0, 255), 4)
            
            cv2.imwrite(saveFolder + '/frame_' + nameCam + '_' + str(count) + '.jpg', img_save)

            cv2.imwrite(saveFolder + '/AnhSeg/frame_' + nameCam + '_' + str(count) + '.jpg', img_save)
            cv2.imwrite(saveFolder + "/AnhSeg/frame_" + nameCam + "_" + str(count) + "_seg.jpg", vis)
            cv2.imwrite(saveFolder + "/AnhSeg/frame_" + nameCam + "_" + str(count) + "_seg_banh.jpg", vis_banhxe)
            cv2.imwrite(saveFolder + "/AnhSeg/frame_" + nameCam + "_" + str(count) + "_seg_vach.jpg", vis_vach)
            if check_dedinh != 0:
                baithi_log.info("IMAGE PROCESS: DEDINH:" + nameCam)
                return 2
            else:
                baithi_log.info("IMAGE PROCESS: DEVACH:" + nameCam)
                return 1

    def draw_poly(  self, 
                    image,
                    pts,
                    is_closed=True,
                    color_bgr=[255, 0, 0], 
                    size=0.01, # 1%
                    line_type=cv2.LINE_AA,
                    is_copy=True):

        assert size > 0
        image = image.copy() if is_copy else image # copy/clone a new image
        # calculate thickness
        h, w = image.shape[:2]
        if size > 0:        
            short_edge = min(h, w)
            thickness = int(short_edge * size)
            thickness = 1 if thickness <= 0 else thickness
        else:
            thickness = -1
        
        # calc x,y in absolute coord
        new_pts = []
        for x, y in pts:
            new_pts.append(
                (x, y)
            )
        
        cv2.polylines(  img=image,
                        pts=[np.int32(new_pts)],
                        isClosed=is_closed,
                        color=color_bgr,
                        thickness=thickness,
                        lineType=line_type,
                        shift=0)
        return image


if __name__ == '__main__':
    a = Segmemtation()
