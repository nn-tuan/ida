import sys
sys.path.append(r".\myLib")
try:
    from threading import Thread
    import tkinter as TK
    from tkinter import messagebox, Text
    from PIL import Image, ImageTk
    from MakeDir import *
    from IDAProcess import IDAProcess as IDA
    from log import *
    from CameraSetup import DrawShapes
    import requests
except:
    print(str(traceback.format_exc()))
    input("ERROR")

def Creat_Root_Logger():
    """
    Khởi tạo Root log cho chương trình. Log của chương trình sẽ được lưu trong thư mục BIN của thư mục gốc chương trình.
    File log tạo mới sẽ ghi đè và xóa hết nội dung nếu có file log cũ tồn tại. Tên file mặc định: Log.log.
    File log cho phép ghi ở mức độ DEBUG theo format: [Năm - tháng - ngày - giờ - phút - giây]:[Mức độ log]:[Nội dung log]
    """
    logging_folder = Creat_Log_Folder()
    logging.basicConfig(filename= logging_folder + LOGNAME, level=logging.DEBUG, filemode='a', format= '[%(asctime)s]:[%(levelname)s]:%(message)s', datefmt= '%Y-%m-%d %H:%M:%S')
    root_log_folder = logging_folder
    return root_log_folder

class Layout(IDA):
    """
    Class này giúp khởi tạo giao diện cho IDA.
    Toàn bộ bố cục, layout, thao tác với giao diện thể hiện trong class này.
    Code Example:\n
        rootloger = Creat_Root_Logger()\n
        layout = Layout(rootloger)\n
        layout.start()\n
    """

    def __init__(self, root_log_folder, geometry='650x530+0+0', title="NEW WINDOWN", bg="white", minsize=[650, 530], maxsize=[1366, 768]):
        IDA.__init__(self)
        self.root = TK.Tk()
        self.root_log_folder = root_log_folder
        self.layout_init(geometry, title, bg, minsize, maxsize)
        self.text = Text(self.root, font=(12))
        self.SetImg = DrawShapes(text=self.text, root=self.root, width=1366, height=720, cursor="cross")
        self.isRS232_run = True

    def layout_init(self, geometry='650x530', title="NEW WINDOWN", bg="white", minsize=[650, 530], maxsize=[1366, 768]):
        self.root.resizable(False, False)
        self.root.geometry(geometry)
        self.root.title(title)
        self.root.configure(background=bg)
        self.root.minsize(minsize[0], minsize[1])
        self.root.maxsize(maxsize[0], maxsize[1])
        

        self.creat_label()
        self.creat_event_callback()

    def creat_img(self):
        img_active   = Image.open("./img/active.png")
        img_deactive = Image.open("./img/deactive.png")
        img_setting  = Image.open("./img/setting.png")

        self.active_img  = ImageTk.PhotoImage(img_active.resize((30,30), Image.ANTIALIAS))
        self.deactive_img= ImageTk.PhotoImage(img_deactive.resize((22,22), Image.ANTIALIAS))
        self.setting_img     = ImageTk.PhotoImage(img_setting.resize((40,40), Image.ANTIALIAS))

    def creat_label(self):
        self.creat_img()
        self.creat_frame_label()
        self.creat_for_camera_label()
        self.creat_tool_label()
        self.creat_button_label()

    def creat_frame_label(self):
        #create frame
        self.info_frame = TK.Frame(self.root, bg='white', width=315, height=150, highlightthickness=1)
        #center = Frame(root, bg='blue', width=450, height=200)
        self.main_frame = TK.Frame(self.root, bg='white', width=610, height=260)
        self.info_frame.place(x=200, y=20)

        self.info_frame.config(highlightbackground = "#619fd6", highlightcolor= "#619fd6")
        self.main_frame.place(x=20, y=200)

    def creat_for_camera_label(self):
        #connect camera button
        self.start_label = TK.Label(self.root, text="Kết nối Cam", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.start_label.place(x=20,y=20,width=100, height=50)

        #frame camera infomation
        camera_1 = TK.Label(self.info_frame, text='Cam 1:', font=('Arial', 13), bg='white')
        camera_1.place(x=10, y=10, width=80, height=50)
        self.camera_1_info = TK.Label(self.info_frame, image=self.deactive_img, bg='white')
        self.camera_1_info.place(x=80, y=10, width=50, height=50)

        camera_2 = TK.Label(self.info_frame, text='Cam 2:', font=('Arial', 13), bg='white')
        camera_2.place(x=180,y=10,width=80, height=50)
        self.camera_2_info = TK.Label(self.info_frame, image=self.deactive_img, bg='white')
        self.camera_2_info.place(x=250,y=10, width=50, height=50)

        camera_3 = TK.Label(self.info_frame, text='Cam 3:', font=('Arial', 13), bg='white')
        camera_3.place(x=10,y=50,width=80, height=50)
        self.camera_3_info = TK.Label(self.info_frame, image=self.deactive_img, bg='white')
        self.camera_3_info.place(x=80,y=50, width=50, height=50)

        camera_4 = TK.Label(self.info_frame, text='Cam 4:', font=('Arial', 13), bg='white')
        camera_4.place(x=180,y=50,width=80, height=50)
        self.camera_4_info = TK.Label(self.info_frame, image=self.deactive_img, bg='white')
        self.camera_4_info.place(x=250,y=50, width=50, height=50)

        camera_5 = TK.Label(self.info_frame, text='Cam 5:', font=('Arial', 13), bg='white')
        camera_5.place(x=10,y=90,width=80, height=50)
        self.camera_5_info = TK.Label(self.info_frame, image=self.deactive_img, bg='white')
        self.camera_5_info.place(x=80,y=90, width=50, height=50)

        camera_6 = TK.Label(self.info_frame, text='Cam 6:', font=('Arial', 13), bg='white')
        camera_6.place(x=180,y=90,width=80, height=50)
        self.camera_6_info = TK.Label(self.info_frame, image=self.deactive_img, bg='white')
        self.camera_6_info.place(x=250,y=90, width=50, height=50)

    def creat_tool_label(self):
        self.setting_label = TK.Label(image=self.setting_img, bg='white')
        self.setting_label.place(x=600, y= 10, width= 40, height= 40)

        self.ready_label = TK.Label(self.root, text="Sẵn sàng", bg="#5b9bd5",fg="white", font=('Arial', 13))
        self.ready_label.place(x=20,y=80,width=100, height=50)

    def creat_button_label(self):
        #frame main menu
        self.label_start_hang_dinh = TK.Label(self.main_frame, text="Bắt đầu bài thi hàng đinh", bg='#5b9bd5', fg='white', font=('Arial', 14))
        self.label_start_hang_dinh.place(x=0, y=0, width=300, height=40)
        self.label_end_hang_dinh = TK.Label(self.main_frame, text="Kết thúc bài thi hàng đinh", bg='#5b9bd5', fg='white', font=('Arial', 14))
        self.label_end_hang_dinh.place(x=320, y=0, width=300, height=40)

        self.label_start_so_8 = TK.Label(self.main_frame, text="Bắt đầu bài thi số 8", bg='#5b9bd5', fg='white', font=('Arial', 14))
        self.label_start_so_8.place(x=0, y=50, width=300, height=40)
        self.label_end_so_8 = TK.Label(self.main_frame, text="Kết thúc bài thi số 8", bg='#5b9bd5', fg='white', font=('Arial', 14))
        self.label_end_so_8.place(x=320, y=50, width=300, height=40)

        self.label_start_thi_ghep = TK.Label(self.main_frame, text="Bắt đầu bài thi Ghép", bg='#5b9bd5', fg='white', font=('Arial', 14))
        self.label_start_thi_ghep.place(x=0, y=100, width=300, height=40)
        self.label_end_thi_ghep = TK.Label(self.main_frame, text="Kết thúc bài thi Ghép", bg='#5b9bd5', fg='white', font=('Arial', 14))
        self.label_end_thi_ghep.place(x=320, y=100, width=300, height=40)

        self.label_start_thi_ziczac = TK.Label(self.main_frame, text="Bắt đầu bài thi Ziczac", bg='#5b9bd5', fg='white', font=('Arial', 14))
        self.label_start_thi_ziczac.place(x=0, y=150, width=300, height=40)
        self.label_end_thi_ziczac = TK.Label(self.main_frame, text="Kết thúc bài thi Ziczac", bg='#5b9bd5', fg='white', font=('Arial', 14))
        self.label_end_thi_ziczac.place(x=320, y=150, width=300, height=40)

        #button finish
        self.finish_label = TK.Label(text='Kết thúc', bg="#5b9bd5",fg="#effff5", font=('Arial', 14))
        self.finish_label.place(x=20,y=430,width=100, height=50)

        #RS232_CallBack
        self.runLable = TK.Label(self.root, text = "")

    def creat_event_callback(self):
        # EventCallBack
        self.setting_label.bind('<Button-1>', self.setting_callback)

        self.start_label.bind("<Button-1>", self.connect_camera_callback)
        self.root.protocol("WM_DELETE_WINDOW", self.exit_windown_callback)

        self.label_start_so_8.bind("<Button-1>", self.start_so8_callback)
        self.label_end_so_8.bind("<Button-1>", self.stop_callback)

        self.label_start_hang_dinh.bind("<Button-1>", self.start_hd_callback)
        self.label_end_hang_dinh.bind("<Button-1>", self.stop_callback)

        self.label_start_thi_ziczac.bind("<Button-1>", self.start_zz_callback)
        self.label_end_thi_ziczac.bind("<Button-1>", self.stop_callback)

        self.label_start_thi_ghep.bind("<Button-1>", self.start_qv_callback)
        self.label_end_thi_ghep.bind("<Button-1>", self.stop_callback)

        self.finish_label.bind("<Button-1>", self.close_windown_callback) #Exit_callback
        self.ready_label.bind("<Button-1>", self.ready_callback)
    
    def setting_callback(self, event):
        self.SetImg.set = True
        self.SetImg.setbuff = True
        # try:
        #     requests.get("http://admin:admin@192.168.0.103/appispmu.cgi?btOK=submit&i_o_md=4&i_o_rlv=1&i_o_fq=0")
        # except:
        #     print("loi cam")
        #     pass
        self.root.geometry('1366x768+10+10')
        self.SetImg.pack()
        root_log.info("Layout: SETTING:")
        pass

    def connect_camera_callback(self, event):
        if not self.is_camchecking:
            if self.rs232.thisinh.Thi != STARTED:
                pCamCheck = Thread(target= self.KiemTraCam)
                pCamCheck.start()
        else:
            root_log.info("Layout: CAM is checking...")
            if self.rs232.SerialWriteStr(KDCAMCHECKING):
                root_log.info("RS232: TRANSMIT: KDCAMCHECKING")
            else:
                root_log.warning("Send data to RS232: ERROR")
                pass
            pass

    def close_windown_callback(self, event):
        root_log.info("LAYOUT: THONG BAO: EXIT")
        if messagebox.askokcancel("Thoát", "Bạn có chắc chắn muốn thoát không?"):
            root_log.info("Force STOP")
            self.force_stop()
            self.root.destroy()
            # os.system("shutdown -r -t 10")
            os._exit(0)
        else:
            root_log.info("LAYOUT: CANCLE")
        pass
    def exit_windown_callback(self):
        root_log.info("LAYOUT: THONG BAO: EXIT")
        if messagebox.askokcancel("Thoát", "Bạn có chắc chắn muốn thoát không?"):
            root_log.info("Force STOP")
            self.force_stop()
            self.root.destroy()
            # os.system("shutdown -r -t 10")
            os._exit(0)
        else:
            root_log.info("LAYOUT: CANCLE")
        pass
    
    def start_bai_thi(self, baithi):
        if not self.rs232.thisinh.Thi == STOPED:
            root_log.warning("LAYOUT: STARTERROR: Bai thi khac dang chay")
            messagebox.showinfo("Thông báo", "Bài thi khác đang diễn ra. Hãy hoàn thành bài thi trước khi chạy bài thi mới")
        elif self.rs232.thisinh.maTS == "":
            root_log.warning("LAYOUT: STARTERROR: Khong co Ma TS")
            messagebox.showinfo("Thông báo", "Chưa có mã thí sinh. Hãy nhập mã thí sinh mới hoặc bấm nút: \"Sẵn Sàng\"")
        elif self.is_camchecking:
            root_log.warning("LAYOUT: STARTERROR: Dang kiem tra CAM")
            messagebox.showinfo("Thông báo", "Đang kiểm tra kết nối cam, xin đợi!")
        else:
            root_log.info("LAYOUT: Da nhan nut START:" + baithi)
            self.set(baithi, "")
            self.LayOutData(START)
        pass
    def start_so8_callback(self, event):
        self.start_bai_thi(BAITHISO8)
        pass
    def start_hd_callback(self, event):
        self.start_bai_thi(BAITHIHANGDINH)
        pass
    def start_zz_callback(self, event):
        self.start_bai_thi(BAITHIZICZAC)
        pass
    def start_qv_callback(self, event):
        self.start_bai_thi(BAITHIQUAYVONG)
        pass
    
    def stop_callback(self, event):
        if self.is_camchecking:
            root_log.warning("LAYOUT: STOPERROR: Dang kiem tra CAM")
            messagebox.showinfo("Thông báo", "Đang kiểm tra kết nối cam, xin đợi!")
        else:
            root_log.info("LAYOUT: Da nhan nut STOP")
            self.LayOutData(STOP)
            pass
        pass
    
    def ready_callback(self, event):
        mathisinh = try_create_folder_result("", self.result_folder)
        self.log_folder = ""
        self.set("", mathisinh)
        root_log.info("LAYOUT: NEW:" + str(mathisinh))
        messagebox.showinfo("Thông báo", "Thí sinh mới đã sẵn sàng:" + mathisinh)
        pass

    def KiemTraCam(self):
        cam_info = [self.camera_1_info, 
                    self.camera_2_info, 
                    self.camera_3_info, 
                    self.camera_4_info, 
                    self.camera_5_info, 
                    self.camera_6_info]

        self.update_seting()
        cam = [self.cam_1, self.cam_2, self.cam_3, self.cam_4, self.cam_5, self.cam_6]
        self.is_camchecking = True #Bao đang check cam

        root_log.info("CAM is checking...")
        self.start_label.config(text= "Xin chờ...")
        if self.rs232.SerialWriteStr(KDCAMCHECKING):
            root_log.info("RS232: TRANSMIT: " + KDCAMCHECKING)
        else:
            root_log.warning("RS232: ERROR: TRANSMIT: " + KDCAMCHECKING)
            pass
        # Bắt đầu check cam
        i = 0
        for item in cam:
            i += 1
            root_log.info("KDCAMCHECK: CAM" + str(i) + " - ID CAM: " + item)
            try:
                http_cam = self.http_cam_covert(item)
                if http_cam != "NONE":
                    print(http_cam + "/applogin.cgi")
                    r = requests.get(http_cam + "/applogin.cgi", timeout= 2)
                    if r.status_code == 200:
                        self.cam_run[i] = cv2.VideoCapture(item)
                        if self.cam_run[i].read()[0] == False:
                            self.result_connect[i - 1] = KDCAMERR + str(i)
                        else:
                            self.result_connect[i - 1] = KDCAMOK + str(i)
                    else:
                        self.result_connect[i - 1] = KDCAMERR + str(i)
                else:
                    self.cam_run[i] = cv2.VideoCapture(item)
                    if self.cam_run[i].read()[0] == False:
                        self.result_connect[i - 1] = KDCAMERR + str(i)
                    else:
                        self.result_connect[i - 1] = KDCAMOK + str(i)

            except:
                self.result_connect[i - 1] = KDCAMERR + str(i)
                pass

            # Hiện thị trạng thái Cam trên màn hình Giao Diện
            if self.result_connect[i - 1].find(KDCAMOK, 0, len(KDCAMOK)) > -1:
                cam_info[i - 1].config(image=self.active_img)
            else:
                cam_info[i - 1].config(image=self.deactive_img)
            self.connect_buff[i - 1] = self.result_connect[i - 1]

        # Gui thong tin Cam qua RS232 sau khi check cam xong
        for result_cam in self.result_connect:
            if self.rs232.SerialWriteStr(result_cam):
                root_log.info("RS232: TRANSMIT: " + result_cam)
            else:
                root_log.warning("Send data to RS232: ERROR!")
                root_log.info(result_cam)
                pass

        self.start_label.config(text= "Kết nối Cam")

        # Kết thúc
        self.is_camchecking = False
        return self.result_connect

    def CameraCheck(self):
        cam_info = [self.camera_1_info, 
                    self.camera_2_info, 
                    self.camera_3_info, 
                    self.camera_4_info, 
                    self.camera_5_info, 
                    self.camera_6_info]
        for i in range(6):
            if self.result_connect[i].find(KDCAMOK, 0, len(KDCAMOK)) > -1:
                cam_info[i].config(image=self.active_img)
            else:
                cam_info[i].config(image=self.deactive_img)
    def DisplayExam(self):
        baithi = [BAITHISO8, BAITHIZICZAC, BAITHIHANGDINH, BAITHIQUAYVONG]
        label = [self.label_start_so_8, self.label_start_thi_ziczac, self.label_start_hang_dinh, self.label_start_thi_ghep]
        if self.rs232.camcheck:
            self.connect_camera_callback("")
            self.rs232.camcheck = False
        if self.rs232.thisinh.Thi == STARTED:
            self.CameraCheck()
            for i in range(4):
                if self.rs232.thisinh.BaiThi == baithi[i]:
                    label[i].config(bg='#ff704d')
        elif self.rs232.thisinh.Thi == STOPED:
            for lb in label:
                lb.config(bg='#5b9bd5')
        
        if self.SetImg.set == False and self.SetImg.setbuff == True:
            self.SetImg.setbuff = False
            self.SetImg.pack_forget()
            self.update_seting()
            self.root.geometry('650x530')
            root_log.info("Layout: SETTING END")
    
    def RS232Run(self):
        if not self.isRS232_run:
            root_log.warning("RS232 ERROR: " +  COMDEFAULT + "! Can't open!")
            if messagebox.askokcancel("Lỗi!", "Lỗi hệ thống RS232: " +  COMDEFAULT + "! Hãy thử kiểm tra lại kết nối RS232 hoặc khởi động lại hệ thống. Bạn có muốn thoát không?"):
                root_log.info("Force STOP")
                self.force_stop()
                self.root.destroy()
                os._exit(0)
            else:
                root_log.warning("RS232 ERROR. Keep Running!")
                messagebox.showinfo("Thông báo", "Hệ thống sẽ chạy mà không có kết nối RS232!")
                self.isRS232_run = True

        self.run()
        self.DisplayExam()

        # Gọi lai hàm này sau 10ms.
        self.runLable.after(10, self.RS232Run)

    def start(self):

        try:
            self.FirstRun()
        except:
            self.isRS232_run = False
            root_log.error(str(traceback.format_exc()))
        try:
            self.RS232Run()
        except:
            root_log.error("LOI HE THONG:" + str(traceback.format_exc()))
        self.root.mainloop()
    
    def mainloop(self):
        self.root.mainloop()
        