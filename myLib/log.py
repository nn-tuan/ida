from setting import *
import logging




baithi_logger = logging.getLogger('IDAProcess')


def CreatLog(file_name, logger, file_mode = 'a', delay = False, info = True):
    global logHandle
    # Print LogFile info to Root_Log
    if info:
        creat_file = "Creat Log File: " + file_name 
        root_log.info(creat_file)

    # create the handler for the main logger
    file_logger = logging.FileHandler(file_name, file_mode, delay= delay)
    NEW_FORMAT = '[%(asctime)s]:[%(levelname)s]:%(message)s'
    file_logger_format = logging.Formatter(NEW_FORMAT, datefmt= '%Y-%m-%d %H:%M:%S')

    # tell the handler to use the above format
    file_logger.setFormatter(file_logger_format)

    # finally, add the handler to the base logger
    logger.addHandler(file_logger)

    # remember that by default, logging will start at 'warning' unless
    # we set it manually
    logger.setLevel(logging.DEBUG)
    if info:
        logger.info("File Created:")
    return file_logger
class RootLogDef():

    def __init__(self, logger):
        self.logger = logger
    def error(self, data):
        self.logger.error(data)
        print(data)

    def exception(self, data):
        self.logger.exception(data)
        print(data)

    def warning(self, data):
        self.logger.warning(data)
        print(data)

    def warn(self, data):
        self.logger.warn(data)
        print(data)

    def info(self, data):
        self.logger.info(data)
        print(data)

    def debug(self, data):
        self.logger.debug(data)
        print(data)



baithi_log = RootLogDef(baithi_logger)
root_log = RootLogDef(logging)