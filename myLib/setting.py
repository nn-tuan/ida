import os, signal
from serial.serialutil import EIGHTBITS, PARITY_NONE, STOPBITS_ONE
import cv2
import time
import traceback

ENVPATH = "./myLib/"

def Find(data, str):
    if (data.find(str, 0, len(str)) > -1):
        return 1
    else:
        return 0

def getenv(line_check):
    result = "NONE"
    try:
        file = open(ENVPATH + ".env", 'r+', encoding= 'utf-8')
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            if(Find(data_file, line_check)):
                result = data_file.split("=")[1]
                result = result.split("\n")[0]
                try:
                    check_roi = result.split(",")
                    for i in range(len(check_roi)):
                        check_roi[i] = int(check_roi[i])
                    result = check_roi
                    break
                except:
                    break
            if(file_cursor == file.tell()):
                break
        pass
    finally:
        file.close()
    # print(result)
    return result

DEBUG_FOLDER  = getenv("DEBUG_FOLDER")

#Thông số cài đặt cho Module RS232
PARITY   = PARITY_NONE
BAUDRATE = 9600
BYTESIZE = EIGHTBITS
STOPBITS = STOPBITS_ONE
COMDEFAULT      = "COM1"

#Các chuỗi cho lệnh COMMAND điều khiển tới RS232
BAITHISO8       = "SO8"
BAITHIZICZAC    = "ZZ"
BAITHIQUAYVONG  = "QV"
BAITHIHANGDINH  = "HD"

START           = "START"
STARTING        = "STARTING"
STARTED         = "STARTED"
DEVACH          = "DEVACH"
DEDINH          = "DEDINH"
STOP            = "STOP"
STOPED          = "STOPED"
STOPPING        = "STOPPING"
STOPOK          = "STOPOK"
ERROR           = "ERROR"
OK              = "OK"
READY           = "READY"

KD              = "KD"
KDCONNECT       = "KDCONNECT?"
KDCONNECTED     = "KDCONNECTED"
KDSYSERROR      = "KDSYSERROR"
KDCAMCHECK      = "KDCAMCHECK"
KDCAMCHECKING   = "KDCAMCHECKING"
KDCAMOK         = "KDCAMOK:"
KDCAMERR        = "KDCAMERR:"

NEWSTART        = "NEWSTART"
NEWOK           = "NEWOK"
NEWERROR        = "NEWERROR"
NEWTHISINH      = "NEWTHISINH"
NEWTHISINHOK    = "NEWTHISINHOK"
NEWTEN          = "NEWTEN"
NEW             = "NEW"
ALLSTOP         = "ALLSTOP"
SHUTDOWN        = "SHUTDOWN"
RESTART         = "RESTART"
CANCLE          = "CANCLE"

COMMAND = [BAITHISO8, 
           BAITHIHANGDINH, 
           BAITHIQUAYVONG, 
           BAITHIZICZAC, 
           KD, 
           NEW, 
           ALLSTOP, 
           SHUTDOWN, 
           RESTART, 
           CANCLE]

# Các thông số cài đặt vị trí lưu File
NAME_CAM_1 = "cam1"
NAME_CAM_2 = "cam2"
NAME_CAM_3 = "cam3"
NAME_CAM_4 = "cam4"
NAME_CAM_5 = "cam5"
NAME_CAM_6 = "cam6"

SO8FOLDER       = "BaiThiSo8"
ZICZACFOLDER    = "BaiThiZicZac"
QUAYVONGFOLDER  = "BaiThiGhep"
HANGDINHFOLDER  = "BaiThiHangDinh"

DEVACHFOLDER      = "\\AnhDeVach\\"
VIDEORECORDFOLDER = "\\VideoBaiThi\\"


#Một số thông số khác:
TIME_ERROR = 2              #Thời gian chờ khi gửi thông tin thí sinh. Quá thời gian phản hồi lỗi.
ISRESIZE = 0                #Có cho phép resize về VIDEO_SIZE
FPS = 25                   #Save FPS (25FPS)
VIDEO_SIZE = (1280, 720)    

FPSIMG    = 3              #Test 3FPS
FPSMAXQUEUE = 1/FPSIMG     #Thời gian truyền một Frame ảnh vào Queue của mỗi cam để xử lý.
TIMEREADER = 0.025         #Thời gian đọc video tối thiếu giữa 2 Frame hình ảnh.

LOGNAME = "/Log.log"      #Tên file Log mặc đinh.
LOGSAVEMAX = 99            #Số file Log được lưu tối đa trong thư mục DEBUG


class SETING():
    def __init__(self):
        self.cam_1 = getenv('CAM_1')
        self.cam_2 = getenv('CAM_2')
        self.cam_3 = getenv('CAM_3')
        self.cam_4 = getenv('CAM_4')
        self.cam_5 = getenv('CAM_5')
        self.cam_6 = getenv('CAM_6')
        self.http_cam1 = "NONE"
        self.http_cam2 = "NONE"
        self.http_cam3 = "NONE"
        self.http_cam4 = "NONE"
        self.http_cam5 = "NONE"
        self.http_cam6 = "NONE"

        """
        # Roi Video mới thu:
        """

        self.roi_segment   = getenv('ROI_SEGMENT')

        self.roi_1_default = getenv('ROI_1_DEFAULT')
        self.roi_2_default = getenv('ROI_2_DEFAULT')
        self.roi_3_default = getenv('ROI_3_DEFAULT')
        self.roi_4_default = getenv('ROI_4_DEFAULT') 
        self.roi_5_default = getenv('ROI_5_DEFAULT')
        self.roi_6_default = getenv('ROI_6_DEFAULT')

        self.result_folder = getenv('RESULT_FOLDER')

    def setting_update(self):
        self.result_folder = getenv('RESULT_FOLDER')
        self.roi_segment   = getenv('ROI_SEGMENT')
        self.roi_1_default = getenv('ROI_1_DEFAULT')
        self.roi_2_default = getenv('ROI_2_DEFAULT')
        self.roi_3_default = getenv('ROI_3_DEFAULT')
        self.roi_4_default = getenv('ROI_4_DEFAULT') 
        self.roi_5_default = getenv('ROI_5_DEFAULT')
        self.roi_6_default = getenv('ROI_6_DEFAULT')
        self.cam_1 = getenv('CAM_1')
        self.cam_2 = getenv('CAM_2')
        self.cam_3 = getenv('CAM_3')
        self.cam_4 = getenv('CAM_4')
        self.cam_5 = getenv('CAM_5')
        self.cam_6 = getenv('CAM_6')

    def http_cam_covert(self, cam):
        http_cam = "NONE"
        if Find(cam, "rtsp://"):
            try:
                cam = cam.replace("rtsp://", "")
                id_pw = cam.split("@")[0]
                ip = cam.split("@")[1].split(":")[0]
                http_cam = "http://" + id_pw + "@" + ip
            except:
                http_cam = "NONE"
        return http_cam
        


